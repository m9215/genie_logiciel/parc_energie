package miage.vswk.controleur;

import miage.vswk.dao.ParametreDAO;
import miage.vswk.metier.Parametre;
import miage.vswk.utils.Texte;
import miage.vswk.utils.menu.Menu;
import miage.vswk.utils.menu.commandes.*;

import static miage.vswk.utils.Texte.afficher;

public class ParametreControleur {

    private ParametreControleur() {
    }

    /**
     * Affiche le menu de gestion des paramètres
     */
    public static void configurationParametre() {

        Parametre parametres = ParametreDAO.getParametres();
        if (parametres == null) {
            PrincipalControleur.lancement();
            return;
        }
        afficherParametres(parametres);

        Menu menu = new Menu("Quel paramètre voulez-vous modifier ?");
        menu.addOption("Prix de l'électricité", new ModifierPrixElectriciteCommande(parametres));
        menu.addOption("Prix de réservation", new ModifierPrixReservationCommande(parametres));
        menu.addOption("Prix de dépassement", new ModifierPrixDepassementCommande(parametres));
        menu.addOption("Prix de dédommagement", new ModifierPrixDedommagementCommande(parametres));
        menu.addOption("Prix de supplément de temps", new ModifierPrixSupplementCommande(parametres));
        menu.addOption("Temps d'attente", new ModifierTempsAttenteCommande(parametres));
        menu.addOption("Temps de location temporaire", new ModifierTempsLocationCommande(parametres));
        menu.launch();
        ParametreDAO.miseAJour(parametres);
        afficherParametres(parametres);
    }

    /**
     * Affiche le parametre donne en parametre
     *
     * @param p Parametre
     */
    private static void afficherParametres(Parametre p) {
        afficher("Paramètres actuels : \n"
                + "Prix de l'électricité : " + p.getPrixElectricite() + Texte.EURO_PAR_HEURE + "\n"
                + "Prix de réservation : " + p.getPrix() + Texte.EURO_PAR_HEURE + "\n"
                + "Prix de dépassement : " + p.getPrixDepassement() + Texte.EURO_PAR_HEURE + "\n"
                + "Prix de dédommagement : " + p.getPrixDedommagement() + Texte.EURO_PAR_HEURE + "\n"
                + "Prix de supplément : " + p.getPrixSupplement() + " € /" + p.getTempsAttente() + " min\n"
                + "Temps d'attente : " + p.getTempsAttente() + " min \n"
                + "Temps de location temporaire : " + p.getTempsLocation() + " h\n"
        );
    }

}
