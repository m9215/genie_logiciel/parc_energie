package miage.vswk.controleur;

import miage.vswk.dao.ReservationDAO;
import miage.vswk.metier.Client;
import miage.vswk.utils.ClientSingleton;
import miage.vswk.utils.menu.Menu;
import miage.vswk.utils.menu.commandes.*;

import static miage.vswk.utils.Texte.afficher;

public class PrincipalControleur {

    private PrincipalControleur() {
    }


    /**
     * Affiche le menu de connexion/inscription
     */
    public static void lancement() {
        Menu menu = new Menu("Que voulez-vous faire ?");
        menu.addOption("S'inscrire", new InscriptionCommande());
        menu.addOption("Se connecter", new ConnexionCommande());

        menu.launch();
        Client client = ClientSingleton.getInstance().getClient();
        afficher("Bienvenue " + client.getNom() + " " + client.getPrenom() + " !\n");
        afficherMenuPrincipal();
    }

    /**
     * Affiche le menu principal
     */
    public static void afficherMenuPrincipal() {
        Menu menu = new Menu("Que voulez-vous faire ?");
        menu.addOption("Effectuer une réservation", new EffectuerReservationCommande());
        getReservationOptions(menu);
        menu.addOption("Prolonger une réservation", new MenuProlongationReservationCommande());
        menu.addOption("Voir mes réservations", new VoirMesReservationsCommande());
        menu.addOption("Voir la disponibilité des bornes", new VoirDisponibiliteBorneCommande());
        menu.addOption("Ajouter un véhicule", new AjouterVehiculeCommande());
        menu.addOption("Mon profil", new ProfilCommande());


        if (ClientSingleton.getInstance().getClient().isEstAdmin()) {
            menu.addOption("Accéder au panneau administrateur", new AccederPanneauAdministrateurCommande());
        }
        menu.addOption("Déconnexion", new DeconnexionCommande());

        menu.launch();

        afficherMenuPrincipal();
    }

    /**
     * Affiche les options de réservation
     *
     * @param menu Menu
     */
    private static void getReservationOptions(Menu menu) {
        Client client = ClientSingleton.getInstance().getClient();

        if (ReservationDAO.avoirReservationCourrante(client.getId()) != null) {
            menu.addOption("Fin d'utilisation de la borne", new FinUtilisationBorneCommande());
            return;
        }
        menu.addOption("Activer une borne avec réservation", new ActiverReservationCommande());
        menu.addOption("Activer une borne sans réservation", new UtiliserBorneSansReservationCommande());
    }
}


