package miage.vswk.controleur;

import miage.vswk.dao.LocationDAO;
import miage.vswk.metier.Location;

import java.util.ArrayList;

public class LocationController {

    private LocationController() {
    }

    public static void checkLocation() {
        ArrayList<Location> locations = (ArrayList<Location>) LocationDAO.getLocationsTerminees();
        for (Location location : locations) {
            LocationDAO.supprimerLocationId(location.getId());
        }
    }
}
