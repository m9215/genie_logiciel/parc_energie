package miage.vswk.controleur;

import miage.vswk.dao.BorneDAO;
import miage.vswk.dao.ParametreDAO;
import miage.vswk.dao.ReservationDAO;
import miage.vswk.dao.VoitureDAO;
import miage.vswk.metier.Borne;
import miage.vswk.metier.Client;
import miage.vswk.metier.Reservation;
import miage.vswk.metier.Voiture;
import miage.vswk.utils.ClientSingleton;
import miage.vswk.utils.Insertion;
import miage.vswk.utils.Texte;
import miage.vswk.utils.menu.Menu;
import miage.vswk.utils.menu.MenuListe;
import miage.vswk.utils.menu.MenuOuiNon;
import miage.vswk.utils.menu.commandes.AjouterAttenteReservationCommande;
import miage.vswk.utils.menu.commandes.AjouterTempsReservationCommande;
import miage.vswk.utils.menu.commandes.RetourCommande;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import static miage.vswk.utils.Texte.afficher;
import static miage.vswk.utils.Texte.afficherSansRetourLigne;

public class ReservationControleur {

    private ReservationControleur() {
    }

    /**
     * Permet d'utiliser une borne sans reservation
     */
    public static void sansReservation() {
        Client client = ClientSingleton.getInstance().getClient();

        Voiture v = recupererVoitureClient();
        if (v == null) {
            return;
        }

        afficher("Veuillez entrer la date de fin d'utilisation :");
        Timestamp dateFin = Insertion.getTimestamp();
        Timestamp dateDebut = Timestamp.valueOf(LocalDateTime.now());

        if (dateFin.before(dateDebut)) {
            afficher("La date de fin doit être supérieure à la date de début");
            return;
        }

        // Borne libre ?
        Borne b = BorneDAO.existeBorneDisponible(dateDebut, dateFin, true);

        if (b == null || !b.getStatutBorne().equals(Texte.DISPONIBLE)) {
            afficher(Texte.NON_BORNE_DISPO);
            return;
        }

        // Création de la réservation
        Reservation reservation = new Reservation(client.getId(), b.getId(), v.getId(), dateDebut, dateFin);


        // TODO : verifier qu'une réservation n'est pas en cours ?


        Reservation reservationSuivie = ReservationDAO.existeReservationSuivie(reservation);
        if (reservationSuivie != null) {
            afficher("Vous avez déjà une réservation qui va commencer dans moins d'une heure, merci de l'éditer ou de revenir à l'horaire indiqué.");
            return;
        }

        if (!verificationHoraire(reservation)) {
            afficher("Merci d'entrée une date postérieure à la date actuelle.");
            return;
        }

        ReservationDAO.ajouterReservation(reservation);
        b.setStatutBorne("Occupée");
        BorneDAO.modifierBorne(b);

        afficher("Vous pouvez vous brancher sur la borne " + b.getId());
    }

    /**
     * Permet d'utiliser une borne avec une réservation
     */
    public static void avecReservation() {
        Client client = ClientSingleton.getInstance().getClient();
        afficher("Veuillez saisir votre plaque d'immatriculation au format XX-000-XX");
        Voiture voiture = new Voiture(client.getId(), Insertion.getPlaqueImmatriculation());
        Reservation reservation = ReservationDAO.getByImmat(client.getId(), voiture.getPlaqueImmatriculation());

        if (reservation != null) {
            assignationBorne(reservation);
            return;
        }

        afficher("Aucune reservation connue trouvee");
        afficher("Veuillez saisir votre numero de reservation");
        reservation = ReservationDAO.getReservation(Insertion.getInt());

        assert reservation != null;
        // Reservation d'un autre client ou dans un autre statut que "En attente"
        if (reservation.getClientId() != client.getId() || !reservation.getStatut().equals("En attente")) {
            afficher("Le numéro de reservation n'est pas valide. \nVeuillez vérifier vos reservations et leur statut via le menu principal.\n");
            return;
        }

        // Réservation pas encore commencée
        if (reservation.getDateDebut().after(Calendar.getInstance().getTime())) {
            afficher("Merci de revenir ultérieurement. \nVotre réservation débute le : " + reservation.getDateDebut() + ". \nN'hesitez pas a verifier vos reservation via le menu principal.\n");
            return;
        }

        if (!verifierTempsAttente(reservation)) {

            afficher("Votre réservation a expirée : votre temps d'attente est révolu. \nLa facturation de votre réservation a été traitée. \n");
            reservation.setPrixFinal(BorneControleur.facturerReservation(reservation, true));
            reservation.setStatut("Terminée");
            ReservationDAO.modifierReservation(reservation);
            return;
        }


        assignationBorne(reservation);

    }

    /**
     * Permet d'assigner une borne à une réservation
     *
     * @param reservation la réservation
     */
    private static void assignationBorne(Reservation reservation) {
        Client client = ClientSingleton.getInstance().getClient();
        if (verificationHoraire(reservation)) {
            // La borne n'est pas déjà connue par la réservation ?
            // MAIS est-ce que la borne est disponible ?

            if (reservation.getStatut().equals(Texte.MANQUEE)) {
                afficher("Veuillez entrer la nouvelle date de fin de votre réservation :");
                reservation.setDateFin(Insertion.getTimestamp());
            }

            Borne borne = BorneDAO.existeBorneDisponible(reservation.getDateDebut(), reservation.getDateFin(), true);
            if (borne != null) {
                // Verification dédommagement
                Double dedommagement = checkDedommagementClient(client);
                if (-dedommagement > 0) {
                    afficher("Remise de " + dedommagement + "€ attribuée \n");
                    reservation.setPrixFinal(dedommagement);
                }

                // Changement du statut de la réservation à "En cours"
                reservation.setStatut("En cours");
                //Mise à jour du numero de la borne dans la réservation
                reservation.setBorneId(borne.getId());
                ReservationDAO.modifierReservation(reservation);
                // Changement de statut de la borne
                BorneControleur.gererOccupation(borne);
                afficher("Vous pouvez vous brancher sur la borne " + borne.getId());
                return;
            }

            if (Objects.equals(reservation.getStatut(), Texte.MANQUEE)) {
                afficher(Texte.NON_BORNE_DISPO);
                return;
            }

            dedommagerClient(reservation);
        }

        // TODO vérification si possibilité d'étendre et proposer en fonction du paramètre

    }

    /**
     * Permet de dédommager un client en cas de problème de réservation
     *
     * @param reservation la réservation
     */
    private static void dedommagerClient(Reservation reservation) {
        Double dedommagement = Objects.requireNonNull(ParametreDAO.getParametres()).getPrixDedommagement();
        afficher("Nous sommes sincérement désolés, mais nous ne pouvons pas assurer votre réservation, suite à un dépassement, nous n'avons plus de borne disponible. \n" +
                "Nous vous invitons à réeffectuer une réservation et nous nous excusons sincérement.\n" +
                "En guise d'excuse, nous allons vous dédommager. \n" +
                "Votre réservation a été annulée. \n" +
                "Et nous vous faisons un geste de " + dedommagement + "€ sur votre visite.\n");

        reservation.setPrixFinal(-dedommagement);
        reservation.setStatut("Annulée");
        ReservationDAO.modifierReservation(reservation);
    }

    /**
     * Permet de checker la valeur du dedommagement, selon le client
     *
     * @param client Client
     * @return double
     */
    static Double checkDedommagementClient(Client client) {
        List<Reservation> reservations = ReservationDAO.getReservationByStatut("Annulée");
        double dedommagement = 0.0;

        for (Reservation reservation : reservations) {
            if (reservation.getClientId() == client.getId() && checkDedommagementReservation(reservation)) {
                dedommagement += reservation.getPrixFinal() - recupererFraisSansDedommagement(reservation);
                reservation.setPrixFinal(0.0);
                ReservationDAO.modifierReservation(reservation);
            }
        }

        return dedommagement;
    }

    /**
     * Permet de checker la valeur du dedommagement, selon la reservation
     *
     * @param reservation Reservation
     * @return double
     */
    public static boolean checkDedommagementReservation(Reservation reservation) {
        if (!reservation.getStatut().equals(Texte.ANNULEE))
            return false;

        // La réservation peut ne pas être négative dans le cadre d'une réservation ayant eu des ajouts de temps d'attente
        Double seuil = recupererFraisSansDedommagement(reservation);
        // Retourne True si la réservation est un cas de dédommagement
        return reservation.getPrixFinal() < seuil;
    }

    /**
     * Permet de récupérer le montant de la réservation sans dedommagement
     *
     * @param reservation la réservation
     * @return double
     */
    public static Double recupererFraisSansDedommagement(Reservation reservation) {
        Double prixSupp = Objects.requireNonNull(ParametreDAO.getParametres()).getPrixSupplement();
        int nbSupp = reservation.getNombreProlongationsAttente() - 1; // -1 car début du compteur à 1
        return nbSupp * prixSupp;
        // On pourrait également retourner la valeur du paramètre de dédommagement, cependant, si modification il y a, problème de rétroactivité
    }

    /**
     * Permet de vérifier si la réservation est dans le bon horaire
     *
     * @param reservation la réservation
     * @return boolean
     */
    private static boolean verificationHoraire(Reservation reservation) {

        Timestamp original = reservation.getDateDebut();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(original.getTime());
        cal.add(Calendar.MINUTE, Objects.requireNonNull(ParametreDAO.getParametres()).getTempsAttente());
        Timestamp later = new Timestamp(cal.getTime().getTime());

        return original.getTime() < later.getTime();
    }

    /**
     * Permet d'effectuer une réservation
     */
    public static void effectuerReservation() {

        Client client = ClientSingleton.getInstance().getClient();
        // Voiture pour laquelle on veut réserver
        Voiture v = recupererVoitureClient();

        // Dates de réservation
        afficher("Veuillez entrer la date de début de votre réservation :");
        Timestamp dateDebut = Insertion.getTimestamp();
        afficher("Veuillez entrer la date de fin de votre réservation :");
        Timestamp dateFin = Insertion.getTimestamp();

        // Borne libre
        Borne b = BorneDAO.existeBorneDisponible(dateDebut, dateFin, false);

        // Création de la réservation

        Reservation reservation = creationReservation(v, b, client, dateDebut, dateFin);

        assert reservation != null;

        Reservation reservationSuivie = ReservationDAO.existeReservationSuivie(reservation);
        if (reservationSuivie != null) {

            // S'il existe une réservation qui va commencer dans moins d'une heure, on demande si on fusionne les réservations
            afficher("Vous ne pouvez pas avoir deux réservations qui se suivent avec moins d'une heure d'intervalle. Souhaitez-vous fusionner les réservations ?");
            boolean result = MenuOuiNon.launch();

            if (result) {

                reservationSuivie = ReservationDAO.existeReservationSuivie(reservation);
                while (reservationSuivie != null) {
                    reservation = fusionnerReservations(reservationSuivie, reservation);
                    assert reservation != null;
                    ReservationDAO.modifierReservation(reservation);
                    reservationSuivie = ReservationDAO.existeReservationSuivie(reservation);
                }
                afficher("La réservation a été fusionnée avec succès.");
            }

            return;
        }

        int idReservation = ReservationDAO.ajouterReservation(reservation);
        if (idReservation == -1) {
            afficher("Création de la réservation avortée.");
            return;
        }
        afficher(">>> Réservation effectuée, elle porte le numéro " + idReservation + "\n");
    }

    /**
     * Permet de creer une réservation
     *
     * @param voiture   Voiture
     * @param borne     Borne
     * @param client    Client
     * @param dateDebut Timestamp
     * @param dateFin   Timestamp
     * @return Reservation
     */
    static Reservation creationReservation(Voiture voiture, Borne borne, Client client, Timestamp dateDebut, Timestamp dateFin) {
        if (voiture == null)
            return null;

        if (borne == null) {
            afficher(Texte.NON_BORNE_DISPO);
            return null;
        }

        // si la date de debut est avant la date de fin
        if (dateDebut.after(dateFin)) {
            afficher("La date de début de la réservation doit être antérieure à la date de fin.");
            return null;
        }

        //si la dateDebut est dans la passé
        if (dateDebut.before(new Timestamp(System.currentTimeMillis()))) {
            afficher("La date de début de la réservation ne doit pas être passée.");
            return null;
        }


        return new Reservation(client.getId(), borne.getId(), voiture.getId(), dateDebut, dateFin);
    }

    /**
     * Permet de recuperer la voiture du client
     *
     * @return Voiture
     */
    private static Voiture recupererVoitureClient() {
        Client client = ClientSingleton.getInstance().getClient();
        // Récupérer toutes les voitures
        ArrayList<Voiture> voitures = (ArrayList<Voiture>) VoitureDAO.rechercheParClient(client.getId());

        // Voiture pour laquelle on veut réserver
        Voiture v;

        // Aucune voiture ?
        if (voitures.isEmpty()) {
            afficher("Voulez vous ajouter une voiture ?");
            if (!MenuOuiNon.launch()) {
                // on quitte le menu
                return null;
            }

            // Ajouter une voiture
            afficher("Veuillez saisir votre plaque d'immatriculation au format XX-000-XX");
            String plaqueImmatriculation = Insertion.getString();

            v = new Voiture(client.getId(), plaqueImmatriculation);
            VoitureDAO.insertion(v);

            return v;

        }
        // Plusieurs voitures ?
        // Menu de choix d'une voiture
        MenuListe<Voiture> menuChoisirVoiture = new MenuListe<>("Choisir une voiture :", voitures);
        v = (Voiture) menuChoisirVoiture.launch();

        return v;
    }

    /**
     * Permet de fusionner deux réservations
     *
     * @param reservation1 Reservation
     * @param reservation2 Reservation
     * @return Reservation
     */
    public static Reservation fusionnerReservations(Reservation reservation1, Reservation reservation2) {

        Timestamp debutFusion = reservation1.getDateDebut().after(reservation2.getDateDebut()) ? reservation2.getDateDebut() : reservation1.getDateDebut();
        Timestamp finFusion = reservation1.getDateFin().before(reservation2.getDateFin()) ? reservation2.getDateFin() : reservation1.getDateFin();
        Timestamp intermediaireFusion = debutFusion.equals(reservation1.getDateDebut()) ? new Timestamp(reservation1.getDateFin().getTime() + 1) : new Timestamp(reservation1.getDateDebut().getTime() - 1);

        Timestamp dateDebutTest = debutFusion.equals(reservation1.getDateDebut()) ? intermediaireFusion : debutFusion;
        Timestamp dateFinTest = finFusion.equals(reservation1.getDateFin()) ? intermediaireFusion : finFusion;

        Reservation existe = ReservationDAO.getReservation(reservation2.getId());
        if (existe != null && existe.getBorneId() == reservation1.getBorneId()) {
            ReservationDAO.supprimerReservation(reservation2.getId());
        }

        // On verifie que la borne sera disponible entre les deux dates
        Borne b = BorneDAO.borneEstDisponible(
                reservation1.getBorneId(),
                dateDebutTest,
                dateFinTest);

        if (b == null) {
            afficher("Aucune borne n'est disponible pour cette fusion de périodes.");
            return null;
        }
        return new Reservation(reservation1.getId(), reservation1.getClientId(), b.getId(), reservation1.getVoitureId(), debutFusion, finFusion, reservation1.getStatut(), reservation1.getPrixFinal());
    }

    /**
     * Permet l'affichage des données d'une réservation
     */
    public static void afficherReservation() {

        Reservation reservation = recupererReservation(false);

        // Affichage de la réservation
        assert reservation != null;
        Voiture voiture = VoitureDAO.recherche(reservation.getVoitureId());
        assert voiture != null;
        afficher("Voiture : " + voiture.getPlaqueImmatriculation());
        afficher("Date de début : " + reservation.getDateDebut());
        afficher("Date de fin : " + reservation.getDateFin());
    }


    /**
     * Permet de recuperer une reservation
     *
     * @param termine boolean
     * @return Reservation
     */
    private static Reservation recupererReservation(boolean termine) {
        // Récupérer toutes les réservations d'un client
        Client client = ClientSingleton.getInstance().getClient();

        ArrayList<Reservation> reservations = (ArrayList<Reservation>) ReservationDAO.getReservationsByClient(client.getId(), termine);

        // Aucune réservation ?
        if (reservations.isEmpty()) {
            afficher("Vous n'avez aucune réservation.");
            return null;
        }

        // Menu de choix d'une réservation
        MenuListe<Reservation> menuChoisirReservation = new MenuListe<>("Choisir une réservation :", reservations);
        Reservation reservation = (Reservation) menuChoisirReservation.launch();

        if (reservation == null) {
            afficher("Vous n'avez pas choisi de réservation.");
            return null;
        }

        return reservation;
    }

    /**
     * Affiche le menu de prolongement d'une reservation
     */
    public static void menuProlongationReservation() {

        Menu menu = new Menu("Que voulez-vous faire ?");
        menu.addOption("Ajouter du temps d'attente (+" + Objects.requireNonNull(ParametreDAO.getParametres()).getTempsAttente() + " min | " + ParametreDAO.getParametres().getPrixSupplement() + "€)", new AjouterAttenteReservationCommande());
        menu.addOption("Ajouter du temps à la réservation", new AjouterTempsReservationCommande());
        menu.addOption("Revenir au menu principal", new RetourCommande());
        menu.launch();

        menu.launch();
    }

    /**
     * Permet de prolonger un temps d'attente à une reservation
     */
    public static void ajouterTempsAttenteReservation() {
        Reservation reservation = recupererReservation(true);
        assert reservation != null;

        // Verifier si on a le droit d'ajouter du temps d'attente à la réservation
        if (!verifierTempsAttente(reservation)) {
            afficher("Vous n'avez pas le droit d'ajouter du temps d'attente à cette réservation.");
            return;
        }

        // Ajouter du temps d'attente à la réservation et maj le prixFinal
        ajouterTempsAttente(reservation);
    }

    /**
     * Permet de verifier si on a le droit d'ajouter du temps d'attente à la réservation
     *
     * @param reservation Reservation
     * @return boolean
     */
    public static boolean verifierTempsAttente(Reservation reservation) {

        // recupérer la date de début de la réservation
        Timestamp dateDebut = reservation.getDateDebut();

        // récupération du nombre de prolongations attente
        int nbProlongationsAttente = reservation.getNombreProlongationsAttente();

        // incrémenter la date de début avec le temps d'attente * le nombre de prolongations attente
        Timestamp finAttente = incrementerHoraire(dateDebut, Objects.requireNonNull(ParametreDAO.getParametres()).getTempsAttente() * nbProlongationsAttente);


        // Retourner vrai si la date finAttente est après la date actuelle sinon faux
        return finAttente.after(Timestamp.valueOf(LocalDateTime.now())) && (finAttente.equals(reservation.getDateFin()) || finAttente.before(reservation.getDateFin()));

    }

    /**
     * Permet d'ajouter du temps d'attente à une réservation
     *
     * @param reservation Reservation
     */
    static void ajouterTempsAttente(Reservation reservation) {

        //Definir le nouveau prix final
        Double prixFinal = reservation.getPrixFinal();
        Double prixSupp = Objects.requireNonNull(ParametreDAO.getParametres()).getPrixSupplement();
        int nbProlongationsAttente = reservation.getNombreProlongationsAttente();
        prixFinal += prixSupp;


        reservation.setNombreProlongationsAttente(nbProlongationsAttente + 1);
        reservation.setPrixFinal(prixFinal);

        // Sauvergarder la réservation en BDD
        ReservationDAO.modifierReservation(reservation);

        Timestamp finAttente = incrementerHoraire(reservation.getDateDebut(), Objects.requireNonNull(ParametreDAO.getParametres()).getTempsAttente() * nbProlongationsAttente);

        afficher("Temps d'attente ajouté. Votre temps d'attente prendra fin le : " + finAttente);
    }

    /**
     * Permet d'ajouter un temps en minutes à un timestamp
     *
     * @param dateDebut Timestamp
     * @param minutes   int
     * @return Timestamp
     */
    static Timestamp incrementerHoraire(Timestamp dateDebut, int minutes) {
        // incrémenter dateDebut avec i minutes
        LocalDateTime ldt = dateDebut.toLocalDateTime();
        ldt = ldt.plusMinutes(minutes);
        return Timestamp.valueOf(ldt);
    }

    /**
     * Permet d'ajouter du temps en fin de réservation
     */
    public static void ajouterTempsFinReservation() {
        Reservation reservation = recupererReservation(true);
        assert reservation != null;
        if (new Timestamp(System.currentTimeMillis() + 30 * 60 * 1000).before(reservation.getDateFin())) {
            afficher("Vous ne pouvez pas prolonger la réservation car il reste plus de 30 minutes avant la fin de la réservation.");
            return;
        }
        if (!reservation.incrementerNombreProlongations()) {
            afficher("Vous ne pouvez pas prolonger plus de 3 fois la réservation.");
            return;
        }
        afficher("Votre réservation se termine actuellement à " + reservation.getDateFin());
        afficherSansRetourLigne("Veuillez entrer la nouvelle date de fin : ");
        Timestamp dateFin = Insertion.getTimestamp();
        if (dateFin.before(reservation.getDateFin())) {
            afficher("La nouvelle date de fin ne peut pas être antérieure à la date de fin actuelle.");
            return;
        }
        if (BorneDAO.borneEstDisponible(reservation.getBorneId(), new Timestamp(reservation.getDateFin().getTime() + 1000), dateFin) == null) {
            afficher("La borne n'est plus disponible pour cette période.");
            return;
        }

        reservation.setDateFin(dateFin);
        ReservationDAO.modifierReservation(reservation);
        afficher("Votre réservation a été prolongée !");
    }

    /**
     * Permet de checker une reservation
     */
    public static void checkReservation() {
        List<Reservation> reservations = ReservationDAO.getReservationByStatut("En attente");

        if (reservations.isEmpty()) {
            return;
        }

        // pour chaque réservation, verifier si sa dateFin et après la date actuelle, si oui, facturer et terminer la reservation
        for (Reservation reservation : reservations) {
            if (reservation.getDateFin().equals(new Timestamp(System.currentTimeMillis()))) {
                reservation.setStatut("Terminée");
                reservation.setPrixFinal(BorneControleur.facturerReservation(reservation, false));

                ReservationDAO.modifierReservation(reservation);

            }
        }

    }

    /**
     * Permet de notifier les utilisateurs de la fin de la réservation
     */
    public static void notificationEcheance() {
        List<Reservation> reservations = ReservationDAO.getReservationByStatut("En cours");

        if (reservations.isEmpty()) {
            return;
        }

        // pour chaque réservation, verifier si sa dateFin et après la date actuelle, si oui, facturer et terminer la reservation
        for (Reservation reservation : reservations) {
            if (reservation.getDateFin().equals(new Timestamp(System.currentTimeMillis()))) {
                envoyerNotificationEcheance(
                        reservation.getClientId(),
                        "La réservation n°" + reservation.getId() + " est arrivée à échéance. Merci de libérer la borne."
                );
            }
        }
    }

    /**
     * Permet d'envoyer une notification à un utilisateur
     *
     * @param clientId int
     * @param s        String
     */
    private static void envoyerNotificationEcheance(int clientId, String s) {
        afficher("Envoi de la notification d'échéance à l'utilisateur n°" + clientId + " :\n\t" + s);
    }

    /**
     * Permet de changer le statut d'une réservation
     */
    public static void finPeriodeAttente() {
        List<Reservation> reservations = ReservationDAO.getReservationByStatut(Texte.EN_ATTENTE);
        for (Reservation reservation : reservations) {
            if (!verifierTempsAttente(reservation)) {
                reservation.setPrixFinal(BorneControleur.facturerReservation(reservation, false));
                reservation.setStatut(Texte.MANQUEE);
                ReservationDAO.modifierReservation(reservation);
            }
        }
    }
}
