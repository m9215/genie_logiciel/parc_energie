package miage.vswk.controleur;

import miage.vswk.dao.LocationDAO;
import miage.vswk.dao.VoitureDAO;
import miage.vswk.metier.Client;
import miage.vswk.metier.Location;
import miage.vswk.metier.Voiture;
import miage.vswk.utils.ClientSingleton;
import miage.vswk.utils.Insertion;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static miage.vswk.utils.Texte.afficher;

public class VoitureControleur {

    private static final Client client = ClientSingleton.getInstance().getClient();

    private VoitureControleur() {
    }

    /**
     * Creer une nouvelle voiture
     *
     * @param isLocation boolean
     */
    public static void insertion(Boolean isLocation) {


        Client client = ClientSingleton.getInstance().getClient();
        afficher("Veuillez saisir votre plaque d'immatriculation au format XX-000-XX");

        String plaque = Insertion.getPlaqueImmatriculation();

        Voiture v = VoitureDAO.recherche(plaque);
        if (v != null) {
            afficher("Vous possédez déjà cette voiture");
            return;
        }

        Voiture voiture = new Voiture(client.getId(), plaque);

        if (Boolean.TRUE.equals(isLocation)) {
            ajouterLocation(voiture);
            return;
        }

        VoitureDAO.insertion(voiture);
        afficher("Voiture créée");
    }

    /**
     * Ajouter une location à une voiture
     *
     * @param v Voiture
     */
    private static void ajouterLocation(Voiture v) {
        if (VoitureDAO.besoinLocation(v.getId(), client.getId())) {
            LocationDAO.ajouterLocation(new Location(client.getId(), v.getId(), Timestamp.valueOf(LocalDateTime.now())));
            afficher("Location ajoutée");
        }
    }
}
