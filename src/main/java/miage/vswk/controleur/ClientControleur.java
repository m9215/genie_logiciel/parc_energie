package miage.vswk.controleur;

import miage.vswk.dao.ClientDAO;
import miage.vswk.dao.ReservationDAO;
import miage.vswk.metier.Client;
import miage.vswk.metier.Reservation;
import miage.vswk.utils.ClientSingleton;
import miage.vswk.utils.Insertion;
import miage.vswk.utils.Texte;
import miage.vswk.utils.menu.MenuOuiNon;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;

import static miage.vswk.utils.Texte.afficher;
import static miage.vswk.utils.Texte.afficherSansRetourLigne;

public class ClientControleur {

    private ClientControleur() {
    }

    public static Client connexion() {
        afficherSansRetourLigne("Veuillez entrer votre mail : ");
        String mail = Insertion.getMail();
        Client client = ClientDAO.recherche(mail);
        if (client == null) {
            afficher("Votre compte n'existe pas.");
            return null;
        }
        return client;
    }

    public static void inscription() {
        afficherSansRetourLigne("Veuillez entrer votre nom : ");
        String nom = Insertion.getString();
        afficherSansRetourLigne("Veuillez entrer votre prénom : ");
        String prenom = Insertion.getString();
        afficherSansRetourLigne("Veuillez entrer votre mail : ");
        String mail = Insertion.getMail();
        afficherSansRetourLigne("Veuillez entrer votre numéro de téléphone : ");
        String numero = Insertion.getNumeroTel();
        afficherSansRetourLigne("Veuillez entrer votre numéro de carte de crédit : ");
        String carte = Insertion.getNumeroCarte();

        Client client = new Client(nom, prenom, mail, numero, carte, false);
        client = ClientDAO.insertion(client);
        ClientSingleton.getInstance().setClient(client);
        if (client == null) {
            afficher("");
            return;
        }
        afficher("Votre compte a été créé avec succès.");

        afficher("Voulez-vous ajouter une voiture ?");
        if (MenuOuiNon.launch()) {
            VoitureControleur.insertion(false);
        }

    }

    public static void envoyerFactureToAllClients () {
        List<Client> clients = ClientDAO.findAll();
        for (Client client : clients) {
            // A envoyer par mail
            afficher(creerFacture(client));
        }
    }

    public static String creerFacture(Client client) {

        StringBuilder sb = new StringBuilder();
        double prixTotal = 0;

        sb.append("Facturation du ").append(LocalDateTime.now().getMonth().minus(1).getValue()).append("/").append(LocalDateTime.now().getYear()).append("\n");
        sb.append(client.getNom()).append(" ").append(client.getPrenom()).append("\n");

        List<Reservation> list = ReservationDAO.getReservationsByClient(client.getId(), false);

        for (Reservation r : list) {
            if (r.getStatut() != null && (r.getStatut().equals(Texte.TERMINEE) || r.getStatut().equals(Texte.MANQUEE))) {
                long timestamp = r.getDateDebut().getTime();
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(timestamp);
                if (cal.get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH)) {
                    sb.append("--------------------------------\n");
                    var prix = r.getPrixFinal();
                    prixTotal += prix;
                    sb.append("Réservation n°").append(r.getId()).append(" (Borne n°").append(r.getBorneId()).append(")").append("\n");
                    sb.append("Effectuée du ").append(r.getDateDebut()).append(" au ").append(r.getDateFin()).append("\n");
                    sb.append("Prix : ").append(prix).append("€\n");
                }
            }

        }
        sb.append("--------------------------------\n");
        sb.append("Total : ").append(prixTotal).append("€");
        return sb.toString();
    }

}
