package miage.vswk.controleur;

import miage.vswk.dao.ReservationDAO;
import miage.vswk.metier.Reservation;
import miage.vswk.utils.Texte;
import miage.vswk.utils.menu.Menu;
import miage.vswk.utils.menu.commandes.*;

import java.text.DecimalFormat;
import java.util.List;

import static miage.vswk.utils.Texte.afficher;

public class AdministrateurControleur {

    private AdministrateurControleur() {
    }

    /**
     * Affiche le panneau d'administration
     */
    public static void afficherPanneauAdministrateur() {
        Menu menu = new Menu("Que voulez-vous faire ?");
        menu.addOption("Configurer les paramètres", new ConfigurerParametreCommande());
        menu.addOption("Changer le statut d'une borne", new ChangerStatutBorneCommande());
        menu.addOption("Ajouter une borne", new AjouterBorneCommande());
        menu.addOption("Afficher les données d'un client", new AfficherDonneesClientCommande());
        menu.addOption("Afficher les recettes", new AfficherRecetteApplication());

        menu.launch();
    }

    /**
     * Affiche la recette de l'application
     */
    public static void afficherRecetteApplication() {
        Double recettes = calculerRecetteApplication();

        DecimalFormat df = new DecimalFormat("#.##");
        afficher("La recette de l'application est de " + df.format(recettes) + "€.");
    }

    /**
     * Calcule les recettes de l'application
     */
    static Double calculerRecetteApplication() {
        List<Reservation> reservationList = ReservationDAO.getReservationByStatut(Texte.TERMINEE);
        reservationList.addAll(ReservationDAO.getReservationByStatut(Texte.MANQUEE));
        reservationList.addAll(ReservationDAO.getReservationByStatut(Texte.ANNULEE));

        double recettes = 0;

        for (Reservation reservation : reservationList) {
            recettes += reservation.getPrixFinal();
        }

        return recettes;
    }
}
