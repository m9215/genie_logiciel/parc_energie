package miage.vswk.controleur;

import miage.vswk.dao.ClientDAO;
import miage.vswk.dao.VoitureDAO;
import miage.vswk.metier.Client;
import miage.vswk.metier.Voiture;
import miage.vswk.utils.ClientSingleton;
import miage.vswk.utils.menu.Menu;
import miage.vswk.utils.menu.MenuListe;
import miage.vswk.utils.menu.commandes.*;

import java.util.List;
import java.util.Objects;

import static miage.vswk.utils.Texte.afficher;

public class ProfilControleur {

    private ProfilControleur() {
    }

    /**
     * Permet le fait de modifier un profil
     */
    public static void modifierProfil() {
        Client client = ClientSingleton.getInstance().getClient();
        Menu menu = new Menu("Quel paramètre voulez-vous modifier ?");
        menu.addOption("Prenom : " + client.getPrenom(), new ModifierPrenomCommande());
        menu.addOption("Nom : " + client.getNom(), new ModifierNomCommande());
        menu.addOption("Mail : " + client.getMail(), new ModifierMailCommande());
        menu.addOption("Telephone portable :" + client.getMobile(), new ModifierMobileCommande());
        menu.addOption("Carte de crédit", new ModifierCreditCommande());
        menu.launch();
        if (ClientDAO.miseAJour(ClientSingleton.getInstance().getClient())) {
            afficher("Informations mises à jour !");
            return;
        }
        afficher("Erreur lors de la mise à jour des informations !");
        afficherMenu();
    }

    /**
     * Permet d'afficher le menu profil
     */
    public static void afficherMenu() {
        Menu menu = new Menu("Que voulez-vous faire ?");
        menu.addOption("Afficher mon profil", new AfficherProfilCommande());
        menu.addOption("Modifier mon profil", new ModifierProfilCommande());
        menu.launch();
    }


    /**
     * Permet d'afficher un profil
     */
    public static void afficherProfil() {
        Client client = ClientSingleton.getInstance().getClient();
        afficherDatas(client);
    }

    /**
     * Selectionne le client a afficher
     */
    public static void afficherProfilClient() {
        List<Client> clients = ClientDAO.findAll();
        if (!verifierListeClient(clients)) {
            return;
        }
        MenuListe<Client> menu = new MenuListe<>("Liste des clients", clients);

        afficherDatas((Client) Objects.requireNonNull(menu.launch()));
    }

    /**
     * Permet de vérifier la liste des clients
     *
     * @param clients Liste des clients
     * @return true si la liste n'est pas vide
     */
    static boolean verifierListeClient(List<Client> clients) {
        if (clients == null || clients.isEmpty()) {
            afficher("Aucun client n'a été trouvé.");
            return false;
        }
        return true;
    }

    /**
     * Permet d'afficher les informations d'un client
     *
     * @param client Client
     */
    private static void afficherDatas(Client client) {
        afficher("Prénom : " + client.getPrenom());
        afficher("Nom : " + client.getNom());
        afficher("Mail : " + client.getMail());
        afficher("Numéro de téléphone : " + client.getMobile());
        afficher("Numéro de carte de crédit : " + client.getCb());
        afficher("Voitures : ");
        for (Voiture v : VoitureDAO.rechercheParClient(client.getId())) {
            afficher(v.getPlaqueImmatriculation());
        }
        afficher("");
    }
}
