package miage.vswk.controleur;

import miage.vswk.dao.BorneDAO;
import miage.vswk.dao.ParametreDAO;
import miage.vswk.dao.ReservationDAO;
import miage.vswk.metier.Borne;
import miage.vswk.metier.Client;
import miage.vswk.metier.Parametre;
import miage.vswk.metier.Reservation;
import miage.vswk.utils.ClientSingleton;
import miage.vswk.utils.Texte;
import miage.vswk.utils.menu.MenuListe;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import static miage.vswk.utils.Texte.afficher;

public class BorneControleur {

    private static final Client client = ClientSingleton.getInstance().getClient();

    private BorneControleur() {
    }

    /**
     * Affiche si au moins une borne est disponible
     */
    public static void afficherDispoBorne() {
        if (isBorneDispo()) {
            afficher("Au moins une borne est disponible.");
            return;
        }
        afficher("Aucune borne n'est disponible.");
    }

    /**
     * Retourne si au moins une borne est disponible
     *
     * @return boolean
     */
    public static boolean isBorneDispo() {
        return BorneDAO.existeBorneDisponible(new Timestamp(System.currentTimeMillis()), new Timestamp(System.currentTimeMillis() + 3600), false) != null;
    }


    /**
     * Gerer les pannes d'une borne
     */
    public static void gererPanne() {
        // Récupérer toutes les bornes
        HashMap<Integer, Borne> bornes = (HashMap<Integer, Borne>) BorneDAO.getBornes();

        // Menu de choix d'une borne
        MenuListe<Borne> menuChoisirBorne = new MenuListe<>("Choisir une borne", new ArrayList<>(bornes.values()));
        Borne b = (Borne) menuChoisirBorne.launch();

        Borne b1 = changerBorneDispoIndispo(b);

        if (b1 != null) {
            afficher(Texte.BORNE_NUMERO + b.getId() + " est maintenant" + b.getStatutBorne().toLowerCase() + ".");
            return;
        }
        afficher("Changement de statut impossible.");
    }

    /**
     * Change le statut d'une borne
     *
     * @param b Borne
     * @return Borne | null
     */
    public static Borne changerBorneDispoIndispo(Borne b) {
        if (b == null) {
            return null;
        }

        // Passage de Disponible à Indisponible
        if (b.getStatutBorne().equals(Texte.DISPONIBLE)) {
            b.setStatutBorne(Texte.INDISPONIBLE);
            BorneDAO.modifierBorne(b);
            return b;
        }

        // Passage de Indisponible à Disponible
        if (b.getStatutBorne().equals(Texte.INDISPONIBLE)) {
            b.setStatutBorne(Texte.DISPONIBLE);
            BorneDAO.modifierBorne(b);
            return b;
        }

        // Autre état
        return null;
    }

    /**
     * Gerer les occupations d'une borne
     *
     * @param b Borne
     * @return boolean
     */
    public static Borne gererOccupation(Borne b) {

        if (b == null) {
            return null;
        }

        // Passage de Disponible à Occupe
        if (b.getStatutBorne().equals(Texte.DISPONIBLE)) {
            b.setStatutBorne(Texte.OCCUPE);
            BorneDAO.modifierBorne(b);
            return b;
        }

        // Passage de Occupe à Disponible
        if (b.getStatutBorne().equals(Texte.OCCUPE)) {
            b.setStatutBorne(Texte.DISPONIBLE);
            BorneDAO.modifierBorne(b);
            return b;
        }

        // Autre état
        return null;
    }


    /**
     * Ajouter une borne
     */
    public static boolean ajouterBorne() {
        if (!BorneDAO.ajouterBorne(new Borne(Texte.DISPONIBLE))) {
            afficher("Erreur lors de l'ajout de la borne.");
            return false;
        }
        afficher("Borne ajoutée.");
        return true;
    }

    /**
     * Permet d'arrêter l'utilisation d'une borne
     */
    public static void arretUtilisationBorne() {

        Reservation reservation = ReservationDAO.avoirReservationCourrante(client.getId());
        assert reservation != null;
        afficher("Début de votre réservation : " + reservation.getDateDebut() + "\n"
                + "Fin de votre réservation : " + reservation.getDateFin());

        Borne borne = BorneDAO.getBorne(reservation.getBorneId());
        assert borne != null;

        double prix = facturerReservation(reservation, true);

        DecimalFormat df = new DecimalFormat("#.##");
        afficher("\n>>> Voici le coût total de votre utilisation de borne : " + df.format(prix) + " €");

        reservation.setPrixFinal(prix);
        reservation.setStatut("Terminée");
        ReservationDAO.modifierReservation(reservation);

        borne.setStatutBorne("Disponible");
        BorneDAO.modifierBorne(borne);
    }

    /**
     * Permet de facturer un depassement
     *
     * @param reservation      Reservation
     * @param heureActuelle    Timestamp
     * @param tarifDepassement double
     * @param display          boolean
     * @return double
     */
    public static double facturerDepassement(Reservation reservation, Timestamp heureActuelle, double tarifDepassement, boolean display) {

        if (reservation == null || heureActuelle == null) {
            return 0;
        }

        if (display)
            afficher("Heure actuelle : " + heureActuelle);

        if (reservation.getDateDebut().compareTo(heureActuelle) < 0) {
            double majoration = calculPrixByTemps(reservation.getDateFin(), heureActuelle, tarifDepassement);
            if (display) {
                DecimalFormat df = new DecimalFormat("#.##");
                afficher("Il y a eu un dépassement.");
                afficher("Voici le tarif de votre dépassement : " + df.format(majoration) + " €");
            }


            return majoration;
        }
        return 0.0;
    }

    /**
     * Permet de facturer le prix de l'électricité
     *
     * @param reservation      Reservation
     * @param heureActuelle    Timestamp
     * @param tarifElectricite double
     * @param display          boolean
     * @return double
     */
    public static double facturerPrixElec(Reservation reservation, Timestamp heureActuelle, double tarifElectricite, boolean display) {

        if (reservation == null || heureActuelle == null || !ReservationControleur.verifierTempsAttente(reservation)) {
            return 0;
        }

        double prixElec = calculPrixByTemps(reservation.getDateDebut(), heureActuelle, tarifElectricite);

        DecimalFormat df = new DecimalFormat("#.##");
        if (display)
            afficher("Voici le tarif pour l'électricité : " + df.format(prixElec) + " €");

        return prixElec;
    }

    /**
     * Permet de facturer le prix de la borne
     *
     * @param reservation      Reservation
     * @param tarifReservation double
     * @param display          boolean
     * @return double
     */
    private static double facturerPrixDuree(Reservation reservation, double tarifReservation, boolean display) {

        if (reservation == null) {
            return 0;
        }

        double prixReservation = calculPrixByTemps(reservation.getDateDebut(), reservation.getDateFin(), tarifReservation);

        DecimalFormat df = new DecimalFormat("#.##");
        if (display)
            afficher("Voici le tarif pour l'utilisation de la borne : " + df.format(prixReservation) + " €");

        return prixReservation;
    }

    /**
     * Permet de facturer la réservation
     *
     * @param reservation Reservation
     * @param display     boolean
     * @return double
     */
    public static double facturerReservation(Reservation reservation, boolean display) {

        if (reservation == null) {
            return 0;
        }

        double prix = reservation.getPrixFinal();

        long datetime = System.currentTimeMillis();
        Timestamp heureActuelle = new Timestamp(datetime);

        Parametre parametres = ParametreDAO.getParametres();
        assert parametres != null;

        prix += facturerDepassement(reservation, heureActuelle, parametres.getPrixDepassement(), display);
        prix += facturerPrixElec(reservation, heureActuelle, parametres.getPrixElectricite(), display);
        prix += facturerPrixDuree(reservation, parametres.getPrix(), display);

        return prix;
    }

    /**
     * Permet de calculer le prix depuis une durée
     *
     * @param heureDebut Timestamp
     * @param heureFin   Timestamp
     * @param prix       double
     * @return double
     */
    public static double calculPrixByTemps(Timestamp heureDebut, Timestamp heureFin, double prix) {

        if (heureDebut == null || heureFin == null) {
            return 0;
        }

        if (heureDebut.compareTo(heureFin) > 0) {
            return 0;
        }

        long interval = heureFin.getTime() - heureDebut.getTime();
        //total heures
        long heures = interval / (60 * 60 * 1000);
        //Ce que la période représente en minutes
        long minutes = (interval % (60 * 60 * 1000)) / (60 * 1000);
        //ce que la période représente en secondes
        long secondes = (interval % (60 * 1000)) / 1000;

        return prix * heures + prix * minutes/60 + prix * secondes/3600;
    }
}
