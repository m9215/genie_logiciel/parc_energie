package miage.vswk.dao;

import miage.vswk.database.ConnexionBDD;
import miage.vswk.metier.Voiture;
import miage.vswk.utils.Texte;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class VoitureDAO {

    private static final Connection CONNEXION = ConnexionBDD.getInstance().getConnexion();
    private static final Logger LOG = Logger.getLogger(VoitureDAO.class.getName());

    private VoitureDAO() {
    }

    /**
     * Ajoute une voiture à la base de données
     *
     * @return voiture
     */
    public static Voiture insertion(Voiture voiture) {
        try (PreparedStatement pstmt = CONNEXION.prepareStatement("""
                        INSERT INTO voiture (client_id, plaque_immatriculation)
                        VALUES (?, ?)
                        """,
                Statement.RETURN_GENERATED_KEYS
        )) {
            pstmt.setInt(1, voiture.getClientId());
            pstmt.setString(2, voiture.getPlaqueImmatriculation());
            pstmt.execute();

            try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    voiture.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Pas d'id généré");
                }
            }
            return voiture;
        } catch (SQLException e) {
            LOG.severe("Erreur lors de l'ajout de la voiture : " + e.getMessage());
        }
        return null;
    }

    /**
     * Récupère une voiture à partir de son id
     *
     * @param id int
     * @return voiture
     */
    public static Voiture recherche(int id) {
        try (PreparedStatement pstmt = CONNEXION.prepareStatement("SELECT * FROM voiture WHERE id = ?")) {
            pstmt.setInt(1, id);
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                return new Voiture(resultSet.getInt("id"), resultSet.getInt(Texte.CLIENT_COLUMN_ID), resultSet.getString(Texte.VOITURE_COLUMN_PLAQUE));
            }
        } catch (SQLException e) {
            LOG.severe(Texte.ERREUR_RECUP_VOITURE + e.getMessage());
        }
        return null;
    }

    /**
     * Recupere l'id d'une voiture à partir de son immatriculation et du client
     *
     * @param idclient        int
     * @param immatriculation String
     * @return Voiture
     */
    public static Voiture getId(int idclient, String immatriculation) {
        try (PreparedStatement pstmt = CONNEXION.prepareStatement("""
                SELECT *
                FROM voiture
                WHERE client_id = ? AND  plaque_immatriculation = ?
                """
        )) {
            pstmt.setInt(1, idclient);
            pstmt.setString(2, immatriculation);
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                return new Voiture(resultSet.getInt("id"), resultSet.getInt(Texte.CLIENT_COLUMN_ID), resultSet.getString(Texte.VOITURE_COLUMN_PLAQUE));
            }
        } catch (SQLException e) {
            LOG.severe(Texte.ERREUR_RECUP_VOITURE + e.getMessage());
        }
        return null;
    }

    /**
     * Recupere la liste des voitures d'un client
     *
     * @param clientId int
     * @return List<Voiture>
     */
    public static List<Voiture> rechercheParClient(int clientId) {
        ArrayList<Voiture> voitures = new ArrayList<>();
        try (PreparedStatement pstmt = CONNEXION.prepareStatement("SELECT * FROM voiture WHERE client_id = ?")) {
            pstmt.setInt(1, clientId);

            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                voitures.add(new Voiture(resultSet.getInt("id"), resultSet.getInt(Texte.CLIENT_COLUMN_ID), resultSet.getString(Texte.VOITURE_COLUMN_PLAQUE)));
            }
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la recherche des voiture : " + e.getMessage());
        }
        return voitures;
    }

    /**
     * Recherche une voiture à partir de son immatriculation
     *
     * @param plaqueImmatriculation String
     * @return Voiture
     */
    public static Voiture recherche(String plaqueImmatriculation) {
        try (PreparedStatement pstmt = CONNEXION.prepareStatement("""
                SELECT *
                FROM voiture
                WHERE plaque_immatriculation = ?
                """
        )) {
            pstmt.setString(1, plaqueImmatriculation);
            ResultSet resultSet = pstmt.executeQuery();

            if (resultSet.next()) {
                return new Voiture(resultSet.getInt("id"), resultSet.getInt(Texte.CLIENT_COLUMN_ID), resultSet.getString(Texte.VOITURE_COLUMN_PLAQUE));
            }
        } catch (SQLException e) {
            LOG.severe(Texte.ERREUR_RECUP_VOITURE + e.getMessage());
        }
        return null;
    }

    /**
     * Verifier si une location est necessaire
     *
     * @param voitureId int
     * @param clientId  int
     * @return boolean
     */
    public static boolean besoinLocation(int voitureId, int clientId) {
        try (PreparedStatement pstmt = CONNEXION.prepareStatement("""
                SELECT *
                FROM VOITURE
                WHERE ID = ? AND CLIENT_ID = ?
                """
        )) {

            pstmt.setInt(1, voitureId);
            pstmt.setInt(2, clientId);

            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                return false;
            }
        } catch (SQLException e) {
            LOG.severe(Texte.ERREUR_RECUP_VOITURE + e.getMessage());
        }
        return true;
    }
}
