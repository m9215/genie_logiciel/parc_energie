package miage.vswk.dao;

import miage.vswk.database.ConnexionBDD;
import miage.vswk.metier.Borne;
import miage.vswk.utils.Texte;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class BorneDAO {

    private static final Connection CONNEXION = ConnexionBDD.getInstance().getConnexion();
    private static final Logger LOG = Logger.getLogger(BorneDAO.class.getName());

    private BorneDAO() {
    }

    /**
     * Ajoute une borne à la base de données
     *
     * @return boolean
     */
    public static boolean ajouterBorne(Borne borne) {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                INSERT INTO BORNE (STATUT_BORNE)
                VALUES (?)
                """)
        ) {
            s.setString(1, borne.getStatutBorne());

            s.execute();
            return true;
        } catch (SQLException e) {
            LOG.severe("Erreur lors de l'ajout d'une borne: " + e.getMessage());
        }
        return false;
    }

    /**
     * Modifie une borne dans la base de données
     */
    public static void modifierBorne(Borne borne) {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                UPDATE BORNE
                SET STATUT_BORNE = ?
                WHERE ID = ?
                """)
        ) {
            s.setString(1, borne.getStatutBorne());
            s.setInt(2, borne.getId());

            s.execute();
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la modification d'une borne: " + e.getMessage());
        }
    }

    /**
     * Permet de recuperer toutes les bornes
     *
     * @return Map<Integer, Borne>
     */
    public static Map<Integer, Borne> getBornes() {
        HashMap<Integer, Borne> bornes = new HashMap<>();
        try (PreparedStatement s = CONNEXION.prepareStatement("SELECT * FROM BORNE")) {
            ResultSet rs = s.executeQuery();

            while (rs.next()) {
                Borne borne = new Borne(rs.getInt("ID"), rs.getString(Texte.BORNE_COLUMN_STATUT));
                bornes.put(borne.getId(), borne);
            }

            return bornes;
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la récupération des bornes: " + e.getMessage());
        }
        return new HashMap<>();
    }

    /**
     * Permet de récuperer une borne par son id
     *
     * @param id int
     * @return Borne
     */
    public static Borne getBorne(int id) {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                SELECT *
                FROM BORNE
                WHERE ID = ?
                """)
        ) {


            s.setInt(1, id);

            ResultSet rs = s.executeQuery();

            if (rs.next()) {
                return new Borne(rs.getInt("ID"), rs.getString(Texte.BORNE_COLUMN_STATUT));
            }
        } catch (SQLException e) {
            LOG.severe(Texte.ERREUR_RECUP_BORNE + e.getMessage());
        }
        return null;
    }

    /**
     * Permet de vérifier si une borne est disponible a un temps donne
     *
     * @param start Timestamp
     * @param end   Timestamp
     * @param isNow boolean
     * @return Borne
     */
    public static Borne existeBorneDisponible(Timestamp start, Timestamp end, boolean isNow) {
        if (isNow)
            return borneDispoNow(start, end);

        return borneDispoAfter(start, end);
    }

    /**
     * Permet de vérifier si une borne est disponible a un temps donne
     *
     * @param start Timestamp
     * @param end   Timestamp
     * @return Borne
     */
    private static Borne borneDispoNow(Timestamp start, Timestamp end) {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                    SELECT *  FROM Borne WHERE ((SELECT COUNT(DISTINCT id) FROM reservation where
                             ((DATE_DEBUT BETWEEN ? AND ?) OR
                             (DATE_FIN BETWEEN ? AND ?) OR
                             (DATE_DEBUT < ? AND DATE_FIN > ?))
                             AND statut IN ('En cours', 'En Attente'))
                              < (SELECT COUNT(ID) FROM BORNE))
                      AND STATUT_BORNE = 'Disponible'
                """)
        ) {
            // CAS N°1 : la date de début cherchée se trouve dans une réservation
            // CAS N°2 : la date de fin cherchée se trouve dans une réservation
            // CAS N°3 : la date de début cherchée se trouve avant la date de début d'une réservation et la date de fin cherchée se trouve après la date de fin d'une réservation

            // NB: la borne ne doit pas être INDISPONIBLE ni être OCCUPEE car on ne connait pas à l'avance la durée d'indisponibilité d'une borne
            Borne rs = getBorneByTimestamp(start, end, s);
            if (rs != null) return rs;

        } catch (SQLException e) {
            LOG.severe(Texte.ERREUR_RECUP_BORNE + e.getMessage());
        }
        return null;
    }

    /**
     * Permet de vérifier si une borne est disponible a un temps donne
     *
     * @param start Timestamp
     * @param end   Timestamp
     * @return Borne
     */
    private static Borne borneDispoAfter(Timestamp start, Timestamp end) {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                   SELECT *  FROM Borne WHERE ID = 1 AND ((SELECT COUNT(DISTINCT id) FROM reservation where
                             ((DATE_DEBUT BETWEEN ? AND ?) OR
                             (DATE_FIN BETWEEN ? AND ?) OR
                             (DATE_DEBUT < ? AND DATE_FIN > ?))
                             AND statut IN ('En cours', 'En Attente'))
                              < (SELECT COUNT(ID) FROM BORNE))
                """)
        ) {
            // CAS N°1 : la date de début cherchée se trouve dans une réservation
            // CAS N°2 : la date de fin cherchée se trouve dans une réservation
            // CAS N°3 : la date de début cherchée se trouve avant la date de début d'une réservation et la date de fin cherchée se trouve après la date de fin d'une réservation

            // NB: la borne ne doit pas être INDISPONIBLE ni être OCCUPEE car on ne connait pas à l'avance la durée d'indisponibilité d'une borne
            Borne rs = getBorneByTimestamp(start, end, s);
            if (rs != null) return rs;

        } catch (SQLException e) {
            LOG.severe(Texte.ERREUR_RECUP_BORNE + e.getMessage());
        }
        return null;
    }

    /**
     * Permet de récuperer une borne par un timestamp
     *
     * @param start Timestamp
     * @param end   Timestamp
     * @param s     PreparedStatement
     * @return Borne
     */
    private static Borne getBorneByTimestamp(Timestamp start, Timestamp end, PreparedStatement s) throws SQLException {
        s.setTimestamp(1, start);
        s.setTimestamp(2, end);

        s.setTimestamp(3, start);
        s.setTimestamp(4, end);

        s.setTimestamp(5, start);
        s.setTimestamp(6, end);

        ResultSet rs = s.executeQuery();

        if (rs.next()) {
            return new Borne(rs.getInt("ID"), rs.getString(Texte.BORNE_COLUMN_STATUT));
        }
        return null;
    }

    /**
     * Permet de vérifier si une borne precise est disponible a un temps donne
     *
     * @param borneId int
     * @param start   Timestamp
     * @param end     Timestamp
     * @return Borne
     */
    public static Borne borneEstDisponible(int borneId, Timestamp start, Timestamp end) {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                    SELECT * FROM BORNE WHERE ID = ? AND ID NOT IN (SELECT BORNE_ID FROM RESERVATION WHERE (
                    (DATE_DEBUT BETWEEN ? AND ?) OR
                    (DATE_FIN BETWEEN ? AND ?) OR
                    (DATE_DEBUT < ? AND DATE_FIN > ?)))
                    AND STATUT_BORNE = 'Disponible'
                """)
        ) {
            // CAS N°1 : la date de début cherchée se trouve dans une réservation
            // CAS N°2 : la date de fin cherchée se trouve dans une réservation
            // CAS N°3 : la date de début cherchée se trouve avant la date de début d'une réservation et la date de fin cherchée se trouve après la date de fin d'une réservation

            // NB: la borne ne doit pas être INDISPONIBLE ou OCCUPEE car on ne connait pas à l'avance la durée d'indisponibilité d'une borne
            s.setInt(1, borneId);

            s.setTimestamp(2, start);
            s.setTimestamp(3, end);

            s.setTimestamp(4, start);
            s.setTimestamp(5, end);

            s.setTimestamp(6, start);
            s.setTimestamp(7, end);

            ResultSet rs = s.executeQuery();

            if (rs.next()) {
                return new Borne(rs.getInt("ID"), rs.getString(Texte.BORNE_COLUMN_STATUT));
            }

        } catch (SQLException e) {
            LOG.severe(Texte.ERREUR_RECUP_BORNE + e.getMessage());
        }
        return null;
    }

}

