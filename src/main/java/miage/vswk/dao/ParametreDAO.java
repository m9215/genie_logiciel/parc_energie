package miage.vswk.dao;

import miage.vswk.database.ConnexionBDD;
import miage.vswk.metier.Parametre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

public class ParametreDAO {

    private static final Connection CONNEXION = ConnexionBDD.getInstance().getConnexion();
    private static final Logger LOG = Logger.getLogger(ParametreDAO.class.getName());

    private ParametreDAO() {
    }

    /**
     * Renvoie les paramètres de la base de données
     *
     * @return Parametre
     */
    public static Parametre getParametres() {
        try (PreparedStatement s = CONNEXION.prepareStatement("SELECT * FROM parametres")) {
            ResultSet rs = s.executeQuery();
            if (rs.next()) {
                return new Parametre(rs.getDouble("prix"), rs.getDouble("prix_depassement"), rs.getDouble("prix_dedommagement"), rs.getInt("temps_attente"), rs.getInt("temps_location"), rs.getDouble("prix_electricite"), rs.getDouble("prix_supplement"));
            }
            return null;

        } catch (SQLException e) {
            LOG.severe("Erreur lors de la récupération du paramètre (prix_electricite) : " + e.getMessage());
        }
        return null;
    }

    /**
     * Modifie les paramètres de la base de données
     */
    public static void miseAJour(Parametre p) {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                UPDATE parametres
                SET prix = ?, prix_depassement = ?, temps_attente = ?, temps_location = ?, prix_dedommagement = ?, prix_electricite = ?
                """
        )) {

            s.setDouble(1, p.getPrix());
            s.setDouble(2, p.getPrixDepassement());
            s.setInt(3, p.getTempsAttente());
            s.setInt(4, p.getTempsLocation());
            s.setDouble(5, p.getPrixDedommagement());
            s.setDouble(6, p.getPrixElectricite());

            s.execute();
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la mise à jour des paramètres : " + e.getMessage());
        }
    }


}