package miage.vswk.dao;

import miage.vswk.database.ConnexionBDD;
import miage.vswk.metier.Location;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

public class LocationDAO {

    private static final Connection CONNEXION = ConnexionBDD.getInstance().getConnexion();
    private static final Logger LOG = Logger.getLogger(LocationDAO.class.getName());

    private LocationDAO() {
    }

    /**
     * Ajoute une location à la base de données
     */
    public static void ajouterLocation(Location location) {

        try (PreparedStatement s = CONNEXION.prepareStatement("""
                INSERT INTO LOCATION (CLIENT_ID, VOITURE_ID, DATE_DEBUT)
                VALUES (?, ?, ?)
                """
        )) {
            s.setInt(1, location.getClientId());
            s.setInt(2, location.getVoitureId());
            s.setTimestamp(3, location.getDateDebut());

            s.execute();
        } catch (SQLException e) {
            LOG.severe("Erreur lors de l'ajout d'une location: " + e.getMessage());
        }
    }

    /**
     * Renvoie les locations terminees
     *
     * @return List<Location>
     */
    public static List<Location> getLocationsTerminees() {
        ArrayList<Location> locations = new ArrayList<>();
        int paramTempsLocation = Objects.requireNonNull(ParametreDAO.getParametres()).getTempsLocation();
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                SELECT l.* FROM location l JOIN reservation r on l.voiture_id = r.voiture_id
                WHERE NOW() > l.date_debut + INTERVAL ? HOUR
                AND r.statut = 'Terminée'
                """
        )) {
            s.setInt(1, paramTempsLocation);
            try (ResultSet rs = s.executeQuery()) {
                while (rs.next()) {
                    locations.add(new Location(
                            rs.getInt("id"),
                            rs.getInt("client_id"),
                            rs.getInt("voiture_id"),
                            rs.getTimestamp("date_debut"
                            )));
                }
            }
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la récupération des locations terminées: " + e.getMessage());
        }
        return locations;
    }

    /**
     * Supprimer une location
     *
     * @param id int
     */
    public static void supprimerLocationId(int id) {
        try (PreparedStatement s = CONNEXION.prepareStatement("DELETE FROM location WHERE id = ?")) {
            s.setInt(1, id);
            s.execute();
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la suppression d'une location: " + e.getMessage());
        }
    }
}
