package miage.vswk.dao;

import miage.vswk.database.ConnexionBDD;
import miage.vswk.metier.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static miage.vswk.utils.Texte.afficher;

public class ClientDAO {

    private static final Connection CONNEXION = ConnexionBDD.getInstance().getConnexion();
    private static final Logger LOG = Logger.getLogger(ClientDAO.class.getName());

    private ClientDAO() {
    }

    /**
     * Ajoute un client à la base de données
     *
     * @return Client
     */
    public static Client insertion(Client client) {

        if (!client.estValide()) {
            return null;
        }

        try (PreparedStatement s = CONNEXION.prepareStatement("""
                        INSERT INTO CLIENT (NOM, PRENOM, MAIL, MOBILE, CARTE_CREDIT, EST_ADMIN)
                        VALUES (?, ?, ?, ?, ?, ?)""",
                Statement.RETURN_GENERATED_KEYS
        )) {
            s.setString(1, client.getNom());
            s.setString(2, client.getPrenom());
            s.setString(3, client.getMail());
            s.setString(4, client.getMobile());
            s.setString(5, client.getCb());
            s.setBoolean(6, client.isEstAdmin());

            s.execute();
            try (ResultSet generatedKeys = s.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    client.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Pas d'id généré");
                }
            }
            return client;

        } catch (SQLIntegrityConstraintViolationException sqlIntegrityConstraintViolationException) {
            afficher("Cette adresse mail est déjà utilisée");
        } catch (SQLException e) {
            LOG.severe("Erreur lors de l'ajout du client: " + e);
        }
        return null;
    }

    /**
     * Modifie un client dans la base de données
     *
     * @return boolean
     */
    public static boolean miseAJour(Client client) {
        if (!client.estValide()) {
            return false;
        }

        try (PreparedStatement s = CONNEXION.prepareStatement("""
                UPDATE CLIENT
                SET NOM = ?, PRENOM = ?, MAIL = ?, MOBILE = ?, CARTE_CREDIT = ?
                WHERE ID = ?"""
        )) {
            s.setString(1, client.getNom());
            s.setString(2, client.getPrenom());
            s.setString(3, client.getMail());
            s.setString(4, client.getMobile());
            s.setString(5, client.getCb());
            s.setInt(6, client.getId());

            s.execute();
            return true;
        } catch (SQLIntegrityConstraintViolationException sqlIntegrityConstraintViolationException) {
            afficher("Cette adresse mail est déjà utilisée");
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la mise à jour du client: " + e.getMessage());
        }
        return false;
    }

    /**
     * Recherche un client dans la base de données
     *
     * @param mail String
     * @return Client
     */
    public static Client recherche(String mail) {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                SELECT *
                FROM CLIENT
                WHERE MAIL = ?"""
        )) {
            s.setString(1, mail);
            ResultSet rs = s.executeQuery();
            if (rs.next()) {
                return new Client(rs.getInt("ID"), rs.getString("NOM"), rs.getString("PRENOM"), rs.getString("MAIL"), rs.getString("MOBILE"), rs.getString("CARTE_CREDIT"), rs.getBoolean("EST_ADMIN"));
            }
            return null;
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la récupération du client: " + e.getMessage());
        }
        return null;
    }

    /**
     * Renvoie tous les clients
     *
     * @return List<Client>
     */
    public static List<Client> findAll() {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                SELECT *
                FROM CLIENT
                WHERE est_admin = false
                """
        )) {
            ResultSet rs = s.executeQuery();
            ArrayList<Client> clients = new java.util.ArrayList<>();
            while (rs.next()) {
                clients.add(new Client(rs.getInt("ID"), rs.getString("NOM"), rs.getString("PRENOM"), rs.getString("MAIL"), rs.getString("MOBILE"), rs.getString("CARTE_CREDIT"), rs.getBoolean("EST_ADMIN")));
            }
            return clients;
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la récupération des clients: " + e.getMessage());
        }
        return new ArrayList<>();
    }
}
