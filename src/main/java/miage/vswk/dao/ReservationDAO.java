package miage.vswk.dao;

import miage.vswk.database.ConnexionBDD;
import miage.vswk.metier.Location;
import miage.vswk.metier.Reservation;
import miage.vswk.metier.Voiture;
import miage.vswk.utils.Texte;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ReservationDAO {

    private static final Connection CONNEXION = ConnexionBDD.getInstance().getConnexion();
    private static final Logger LOG = Logger.getLogger(ReservationDAO.class.getName());

    private ReservationDAO() {
    }

    /**
     * Ajoute une location à la base de données
     *
     * @return int
     */
    public static int ajouterReservation(Reservation reservation) {
        // Si aucun lien n'existe entre la voiture et le client...
        if (VoitureDAO.besoinLocation(reservation.getVoitureId(), reservation.getClientId())) {
            // ... on ajoute une réservation
            LocationDAO.ajouterLocation(new Location(reservation.getClientId(), reservation.getVoitureId(), reservation.getDateDebut()));
        }

        // Verifier si le temps est correct
        if (reservation.getDateDebut().after(reservation.getDateFin())) {
            return -1;
        }

        try (PreparedStatement s = CONNEXION.prepareStatement("""
                INSERT INTO RESERVATION (CLIENT_ID, BORNE_ID, VOITURE_ID, DATE_DEBUT, DATE_FIN, STATUT, nombre_prolongation)
                VALUES (?, ?, ?, ?, ?, ?, 0)"""
        )) {
            s.setInt(1, reservation.getClientId());
            s.setInt(2, reservation.getBorneId());
            s.setInt(3, reservation.getVoitureId());
            s.setTimestamp(4, reservation.getDateDebut());
            s.setTimestamp(5, reservation.getDateFin());
            s.setString(6, reservation.getStatut());

            s.execute();
            return getIdReservation(reservation);
        } catch (SQLException e) {
            LOG.severe("Erreur lors de l'ajout d'une réservation: " + e.getMessage());
            return -1;
        }

    }

    /**
     * Renvoie l'id d'une reservation
     *
     * @param reservation Reservation
     * @return int
     */
    private static int getIdReservation(Reservation reservation) {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                SELECT ID FROM RESERVATION WHERE CLIENT_ID = ? AND BORNE_ID = ? AND VOITURE_ID = ? AND DATE_DEBUT = ? AND DATE_FIN = ? AND STATUT = ? 
                """
        )) {
            s.setInt(1, reservation.getClientId());
            s.setInt(2, reservation.getBorneId());
            s.setInt(3, reservation.getVoitureId());
            s.setTimestamp(4, reservation.getDateDebut());
            s.setTimestamp(5, reservation.getDateFin());
            s.setString(6, reservation.getStatut());

            ResultSet rs = s.executeQuery();
            if (rs.next())
                return rs.getInt("ID");
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la récupération de l'id de la réservation: " + e.getMessage());
            return -1;
        }
        return -1;
    }

    /**
     * Renvoie une réservation à partir de son id
     *
     * @param id int
     * @return Reservation
     */
    public static Reservation getReservation(int id) {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                SELECT *
                FROM RESERVATION
                WHERE ID = ?"""
        )) {
            s.setInt(1, id);

            ResultSet rs = s.executeQuery();

            if (rs.next()) {
                return new Reservation(rs.getInt("ID"), rs.getInt(Texte.CLIENT_COLUMN_ID), rs.getInt(Texte.BORNE_COLUMN_ID), rs.getInt(Texte.VOITURE_COLUMN_ID), rs.getTimestamp(Texte.DATE_DEBUT), rs.getTimestamp(Texte.DATE_FIN), rs.getString(Texte.STATUT), rs.getDouble(Texte.RESERVATION_COLUMN_PRIX_FINAL), rs.getInt("NOMBRE_PROLONGATION"), rs.getInt("NOMBRE_PROLONGATION_ATTENTE"));
            }
        } catch (SQLException e) {
            LOG.severe(Texte.ERREUR_RECUP_RESERVATION + e.getMessage());
        }
        return null;
    }

    /**
     * Renvoie toutes les reservations d'un client
     *
     * @param id       id du client
     * @param terminee boolean
     * @return List<Reservation>
     */
    public static List<Reservation> getReservationsByClient(int id, boolean terminee) {
        String sql = "SELECT * FROM RESERVATION WHERE CLIENT_ID = ?";
        if (terminee) {
            sql += "AND STATUT NOT IN ('Terminée', 'Manquée', 'Annulée')";
        }
        try (PreparedStatement s = CONNEXION.prepareStatement(sql)) {
            s.setInt(1, id);

            ResultSet rs = s.executeQuery();

            ArrayList<Reservation> reservations = new ArrayList<>();

            while (rs.next()) {
                reservations.add(new Reservation(rs.getInt("ID"), rs.getInt(Texte.CLIENT_COLUMN_ID), rs.getInt(Texte.BORNE_COLUMN_ID), rs.getInt(Texte.VOITURE_COLUMN_ID), rs.getTimestamp(Texte.DATE_DEBUT), rs.getTimestamp(Texte.DATE_FIN), rs.getString(Texte.STATUT), rs.getDouble(Texte.RESERVATION_COLUMN_PRIX_FINAL), rs.getInt("NOMBRE_PROLONGATION"), rs.getInt("NOMBRE_PROLONGATION_ATTENTE")));
            }
            return reservations;
        } catch (SQLException e) {
            LOG.severe(Texte.ERREUR_RECUP_RESERVATION + e.getMessage());
        }
        return new ArrayList<>();
    }

    /**
     * Modifier une reservation
     *
     * @param reservation Reservation
     */
    public static void modifierReservation(Reservation reservation) {

        // Verifier que la reservation existe
        Reservation r = getReservation(reservation.getId());
        if (r == null) {
            return;
        }

        if (!reservation.estValide()) return;

        try (PreparedStatement s = CONNEXION.prepareStatement("""
                UPDATE RESERVATION SET BORNE_ID = ?, DATE_DEBUT = ?, DATE_FIN = ?, STATUT = ?, PRIX_FINAL = ?, nombre_prolongation = ?, NOMBRE_PROLONGATION_ATTENTE = ? WHERE ID = ?
                """
        )) {
            s.setInt(1, reservation.getBorneId());
            s.setTimestamp(2, reservation.getDateDebut());
            s.setTimestamp(3, reservation.getDateFin());
            s.setString(4, reservation.getStatut());
            s.setDouble(5, reservation.getPrixFinal());
            s.setDouble(6, reservation.getNombreProlongations());
            s.setInt(7, reservation.getNombreProlongationsAttente());
            s.setInt(8, reservation.getId());

            s.execute();
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la modification d'une réservation: " + e.getMessage());
        }
    }

    /**
     * Renvoie une reservation par son immatriculation
     *
     * @param idclient        int
     * @param immatriculation String
     * @return Reservation
     */
    public static Reservation getByImmat(int idclient, String immatriculation) {

        Voiture voiture = VoitureDAO.getId(idclient, immatriculation);
        if (voiture != null) {
            try (PreparedStatement s = CONNEXION.prepareStatement("""
                    SELECT *
                    FROM RESERVATION
                    WHERE client_id = ?
                        AND voiture_id = ?
                        AND ? BETWEEN DATE_DEBUT AND DATE_FIN
                    """
            )) {
                s.setInt(1, idclient);
                s.setInt(2, voiture.getId());
                s.setTimestamp(3, Timestamp.valueOf(LocalDateTime.now()));
                Reservation rs = getReservation(s);
                if (rs != null) return rs;
            } catch (SQLException e) {
                LOG.severe(Texte.ERREUR_RECUP_RESERVATION + e.getMessage());
            }

        }
        return null;
    }

    /**
     * Renvoie une reservation
     *
     * @param s PreparedStatement
     * @return Reservation
     * @throws SQLException SQLException
     */
    private static Reservation getReservation(PreparedStatement s) throws SQLException {
        ResultSet rs = s.executeQuery();
        if (rs.next()) {
            return new Reservation(rs.getInt("ID"), rs.getInt(Texte.CLIENT_COLUMN_ID), rs.getInt(Texte.BORNE_COLUMN_ID), rs.getInt(Texte.VOITURE_COLUMN_ID), rs.getTimestamp(Texte.DATE_DEBUT), rs.getTimestamp(Texte.DATE_FIN), rs.getString(Texte.STATUT), rs.getDouble(Texte.RESERVATION_COLUMN_PRIX_FINAL));
        }
        return null;
    }


    /**
     * Verifie si une reservation est suivie par une autre
     *
     * @param reservation Reservation
     * @return Reservation | null
     */
    public static Reservation existeReservationSuivie(Reservation reservation) {

        Timestamp dateDebut = reservation.getDateDebut();
        Timestamp dateDebutMinus1H = new Timestamp(dateDebut.getTime() - 3600000 + 1);

        Timestamp dateFin = reservation.getDateFin();
        Timestamp dateFinPlus1H = new Timestamp(dateFin.getTime() + 3600000 - 1);

        try (PreparedStatement s = CONNEXION.prepareStatement("""
                SELECT *
                FROM RESERVATION
                WHERE CLIENT_ID = ? AND ID != ?
                    AND ((DATE_DEBUT BETWEEN ? AND ? OR DATE_FIN BETWEEN ? AND ?) OR (? BETWEEN DATE_DEBUT AND DATE_FIN OR ? BETWEEN DATE_DEBUT AND DATE_FIN) OR (DATE_DEBUT <= ? AND DATE_FIN >= ?))
                """
        )) {
            s.setInt(1, reservation.getClientId());
            s.setInt(2, reservation.getId());

            s.setTimestamp(3, dateFin);
            s.setTimestamp(4, dateFinPlus1H);
            s.setTimestamp(5, dateDebutMinus1H);
            s.setTimestamp(6, dateDebut);

            s.setTimestamp(7, dateDebut);
            s.setTimestamp(8, dateFin);

            s.setTimestamp(9, dateDebut);
            s.setTimestamp(10, dateFin);

            Reservation rs = getReservation(s);
            if (rs != null) return rs;
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la vérification de la réservation suivie: " + e.getMessage());
        }
        return null;
    }

    /**
     * Verifie si un client a une reservation en cours
     *
     * @param idClient int
     * @return Reservation
     */
    public static Reservation avoirReservationCourrante(int idClient) {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                SELECT *
                FROM RESERVATION
                WHERE client_id = ?
                    AND statut = 'En cours'
                """
        )) {
            s.setInt(1, idClient);

            Reservation rs = getReservation(s);
            if (rs != null) return rs;
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la récupération d'une réservation courrante d'un client : " + e.getMessage());
        }
        return null;
    }


    /**
     * Supprimer une reservation par son id
     *
     * @param id int
     */
    public static void supprimerReservation(int id) {
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                DELETE FROM RESERVATION WHERE ID = ?
                """
        )) {
            s.setInt(1, id);

            s.execute();
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la suppression d'une réservation: " + e.getMessage());
        }
    }

    /**
     * récupérer la liste de toutes les réservations par statut
     *
     * @param statut String
     * @return List<Reservation>
     */
    public static List<Reservation> getReservationByStatut(String statut) {
        List<Reservation> liste = new ArrayList<>();
        try (PreparedStatement s = CONNEXION.prepareStatement("""
                SELECT *
                FROM RESERVATION
                WHERE STATUT = ?
                """
        )) {
            s.setString(1, statut);

            ajouterListeReservations(liste, s);
        } catch (SQLException e) {
            LOG.severe("Erreur lors de la récupération des réservations par statut : " + e.getMessage());
        }
        return liste;
    }

    /**
     * Permet de creer une liste de reservation
     *
     * @param liste List<Reservation>
     * @param s     PreparedStatement
     * @throws SQLException SQLException
     */
    private static void ajouterListeReservations(List<Reservation> liste, PreparedStatement s) throws SQLException {
        ResultSet rs = s.executeQuery();
        while (rs.next()) {
            liste.add(new Reservation(rs.getInt("ID"), rs.getInt(Texte.CLIENT_COLUMN_ID), rs.getInt(Texte.BORNE_COLUMN_ID), rs.getInt(Texte.VOITURE_COLUMN_ID), rs.getTimestamp(Texte.DATE_DEBUT), rs.getTimestamp(Texte.DATE_FIN), rs.getString(Texte.STATUT), rs.getDouble(Texte.RESERVATION_COLUMN_PRIX_FINAL)));
        }
    }

}
