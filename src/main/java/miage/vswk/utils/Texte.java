package miage.vswk.utils;

public class Texte {

    public static final String ERREUR_SUPPRESSION_VOITURE = "Erreur lors de la suppression de la voiture : ";

    private Texte() {
    }

    public static void afficher(String texte) {
        System.out.println(texte);
    }

    public static void afficherSansRetourLigne(String texte) {
        System.out.print(texte);
    }

    public static final String DISPONIBLE = "Disponible";

    public static final String INDISPONIBLE = "Indisponible";

    public static final String EN_ATTENTE = "En attente";

    public static final String BORNE_NUMERO = "Borne n°";

    public static final String EURO_PAR_HEURE = " € /h ";

    public static final String BORNE_COLUMN_STATUT = "STATUT_BORNE";

    public static final String ERREUR_RECUP_BORNE = "Erreur lors de la récupération de la borne: ";

    public static final String ERREUR_RECUP_RESERVATION = "Erreur lors de la récupération d'une réservation: ";

    public static final String ERREUR_RECUP_VOITURE = "Erreur lors de la recherche de la voiture : ";


    public static final String DATE_DEBUT = "DATE_DEBUT";

    public static final String DATE_FIN = "DATE_FIN";

    public static final String VOITURE_COLUMN_ID = "VOITURE_ID";

    public static final String BORNE_COLUMN_ID = "BORNE_ID";

    public static final String CLIENT_COLUMN_ID = "CLIENT_ID";

    public static final String STATUT = "STATUT";

    public static final String RESERVATION_COLUMN_PRIX_FINAL = "PRIX_FINAL";

    public static final String VOITURE_COLUMN_PLAQUE = "PLAQUE_IMMATRICULATION";

    public static final String OCCUPE = "Occupée";

    public static final String TERMINEE = "Terminée";

    public static final String MANQUEE = "Manquée";
    public static final String ANNULEE = "Annulée";

    public static final String NON_BORNE_DISPO = "Aucune borne n'est disponible pour cette période.";
}
