package miage.vswk.utils;

import miage.vswk.metier.Client;

public class ClientSingleton {
    private static ClientSingleton instance = null;
    private Client client = null;

    public static ClientSingleton getInstance() {
        if (instance == null) {
            instance = new ClientSingleton();
        }
        return instance;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client c) {
        client = c;
    }
}
