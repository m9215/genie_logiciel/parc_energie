package miage.vswk.utils;

import java.sql.Timestamp;
import java.util.Scanner;
import java.util.regex.Pattern;

import static miage.vswk.utils.Texte.afficher;
import static miage.vswk.utils.Texte.afficherSansRetourLigne;

public class Insertion {
    private static final Pattern REGEX_MAIL_VALIDE =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static final Pattern REGEX_PLAQUE_IMMATRICULATION =
            Pattern.compile("^[A-Z]{2}[-][0-9]{3}[-][A-Z]{2}$");

    private static final Pattern REGEX_NUMERO_TELEPHONE =
            Pattern.compile("^\\d{2}([-. ]?\\d{2}){4}$");
    private static final Pattern REGEX_NUMERO_CARTE =
            Pattern.compile("\\d{4}(\\s\\d{4}){4}");


    private Insertion() {
    }


    public static int getInt() {
        try {
            Scanner sc = new Scanner(System.in);
            return sc.nextInt();
        } catch (Exception e) {
            afficher("Merci de rentrer un nombre entier");
            return getInt();
        }
    }

    public static double getDouble() {
        try {
            Scanner sc = new Scanner(System.in);
            return sc.nextDouble();
        } catch (Exception e) {
            afficher("Merci de rentrer un nombre");
            return getDouble();
        }
    }

    public static String getString() {
        try {
            Scanner sc = new Scanner(System.in);
            return sc.nextLine();
        } catch (Exception e) {
            afficher("Merci de rentrer une chaine de caractères");
            return getString();
        }
    }

    public static Timestamp getTimestamp() {
        try {
            Scanner sc = new Scanner(System.in);
            return Timestamp.valueOf(sc.nextLine() + ":00");
        } catch (Exception e) {
            afficher("Merci de rentrer une date valide : YYYY-MM-DD hh:ii");
        }
        return getTimestamp();
    }

    public static String getNumeroTel(){
        String numero = getString();
        if(!verifNumeroTel(numero)) {
            return getNumeroTel();
        }
        return numero;
    }

    public static boolean verifNumeroTel(String numero) {
        if (numero == null) {
            return false;
        }
        if (!REGEX_NUMERO_TELEPHONE.matcher(numero).find()){
            afficher("Numéro de téléphone invalide. Il doit être du format XX.XX.XX.XX.XX");
            return false;
        }
        return true;
    }

    public static String getNumeroCarte(){
        String numero = getString();
        if(!verifNumeroCarte(numero)) {
            return getNumeroCarte();
        }
        return numero;
    }

    public static boolean verifNumeroCarte(String numero) {
        if (numero == null) {
            return false;
        }
        if (!REGEX_NUMERO_CARTE.matcher(numero).find()){
            afficher("Numero de carte bancaire invalide. Il doit, par exemple, être au format : XXXX XXXX XXXX XXXX XXXX");
            return false;
        }
        return true;
    }

    public static String getPlaqueImmatriculation(){
        String plaque = getString();
        if(!verifPlaqueImmat(plaque)) {
            return getPlaqueImmatriculation();
        }
        return plaque;
    }

    public static boolean verifPlaqueImmat(String plaque) {
        if (plaque == null) {
            return false;
        }
        if (!REGEX_PLAQUE_IMMATRICULATION.matcher(plaque).find()){
            afficher("Plaque d'immatriculation invalide");
            return false;
        }
        return true;
    }

    public static String getMail(){
        String mail = getString();
        if (!verifMail(mail)) {
            return getMail();
        }
        return mail;
    }

    public static boolean verifMail(String mail) {
        if (mail == null) {
            return false;
        }
        if (!REGEX_MAIL_VALIDE.matcher(mail).find()) {
            afficherSansRetourLigne("Merci de rentrer une adresse mail valide : ");
            return false;
        }
        return true;
    }


}
