package miage.vswk.utils.menu;

import miage.vswk.utils.Insertion;

import java.util.*;

import static miage.vswk.utils.Texte.afficher;
import static miage.vswk.utils.Texte.afficherSansRetourLigne;

public class Menu {
    Map<String, Commande> commands = new LinkedHashMap<>();
    private final String name;

    public Menu(String name) {
        this.name = name;
    }

    public void addOption(String key, Commande commande) {
        commands.put(key, commande);
    }

    public void launch() {
        afficher(name + " :");
        Map<Integer, Commande> options = new LinkedHashMap<>();
        int i = 1;
        for (Map.Entry<String, Commande> entry : commands.entrySet()) {
            afficher("\t[" + i + "] : " + entry.getKey());
            options.put(i, entry.getValue());
            i++;
        }

        afficherSansRetourLigne("Votre choix : ");
        int choix = Insertion.getInt();
        try {
            options.get(choix).execute();
        } catch (NullPointerException e) {
            afficher("Merci de choisir une option valide.\n");
            this.launch();
        }
    }
}
