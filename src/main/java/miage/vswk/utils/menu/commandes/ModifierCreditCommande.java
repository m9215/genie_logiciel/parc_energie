package miage.vswk.utils.menu.commandes;

import miage.vswk.utils.ClientSingleton;
import miage.vswk.utils.Insertion;
import miage.vswk.utils.menu.Commande;

import static miage.vswk.utils.Texte.afficher;

public class ModifierCreditCommande implements Commande {
    @Override
    public void execute() {
        afficher("Veuillez insérer votre carte de credit : ");
        ClientSingleton.getInstance().getClient().setCb(Insertion.getNumeroCarte());
    }
}
