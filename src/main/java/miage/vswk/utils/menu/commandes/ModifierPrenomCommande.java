package miage.vswk.utils.menu.commandes;

import miage.vswk.utils.ClientSingleton;
import miage.vswk.utils.Insertion;
import miage.vswk.utils.menu.Commande;

import static miage.vswk.utils.Texte.afficher;

public class ModifierPrenomCommande implements Commande {
    @Override
    public void execute() {
        afficher("Veuillez insérer votre prénom :");
        ClientSingleton.getInstance().getClient().setPrenom(Insertion.getString());
    }
}
