package miage.vswk.utils.menu.commandes;

import miage.vswk.metier.Parametre;
import miage.vswk.utils.Insertion;
import miage.vswk.utils.menu.Commande;

import static miage.vswk.utils.Texte.afficher;

public class ModifierTempsAttenteCommande implements Commande {
    private final Parametre parametres;

    public ModifierTempsAttenteCommande(Parametre parametres) {
        this.parametres = parametres;
    }

    @Override
    public void execute() {
        afficher("Veuillez saisir le nouveau temps d'attente :");
        parametres.setTempsAttente(Insertion.getInt());
    }
}
