
package miage.vswk.utils.menu.commandes;

import miage.vswk.metier.Parametre;
import miage.vswk.utils.Insertion;
import miage.vswk.utils.menu.Commande;

import static miage.vswk.utils.Texte.afficher;

public class ModifierPrixSupplementCommande implements Commande {
    private final Parametre parametres;

    public ModifierPrixSupplementCommande(Parametre parametres) {
        this.parametres = parametres;
    }

    @Override
    public void execute() {
        afficher("Veuillez saisir le nouveau prix de supplément lors d'un ajout de temps :");
        parametres.setPrixSupplement(Insertion.getDouble());
    }
}
