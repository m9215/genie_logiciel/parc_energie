package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.BorneControleur;
import miage.vswk.utils.menu.Commande;

public class ChangerStatutBorneCommande implements Commande {
    @Override
    public void execute() {
        BorneControleur.gererPanne();
    }
}
