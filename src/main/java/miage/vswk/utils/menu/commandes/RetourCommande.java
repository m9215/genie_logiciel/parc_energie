package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.PrincipalControleur;
import miage.vswk.utils.menu.Commande;


public class RetourCommande implements Commande {
    @Override
    public void execute() {
        PrincipalControleur.afficherMenuPrincipal();
    }
}