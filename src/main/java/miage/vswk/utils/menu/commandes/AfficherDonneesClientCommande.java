package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.ProfilControleur;
import miage.vswk.utils.menu.Commande;

public class AfficherDonneesClientCommande implements Commande {
    @Override
    public void execute() {
        ProfilControleur.afficherProfilClient();
    }
}
