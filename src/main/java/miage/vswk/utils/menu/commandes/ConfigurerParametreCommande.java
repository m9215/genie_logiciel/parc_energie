package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.ParametreControleur;
import miage.vswk.utils.menu.Commande;

public class ConfigurerParametreCommande implements Commande {

    @Override
    public void execute() {
        ParametreControleur.configurationParametre();
    }
}
