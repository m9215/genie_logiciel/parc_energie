package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.ProfilControleur;
import miage.vswk.utils.menu.Commande;

public class ModifierProfilCommande implements Commande {
        @Override
        public void execute() {
            ProfilControleur.modifierProfil();
        }

}
