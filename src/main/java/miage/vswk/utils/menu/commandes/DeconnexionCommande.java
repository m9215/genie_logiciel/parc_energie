package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.PrincipalControleur;
import miage.vswk.utils.ClientSingleton;
import miage.vswk.utils.menu.Commande;

public class DeconnexionCommande implements Commande {
    @Override
    public void execute() {
        ClientSingleton.getInstance().setClient(null);
        PrincipalControleur.lancement();
    }
}
