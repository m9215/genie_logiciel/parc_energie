package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.ReservationControleur;
import miage.vswk.utils.menu.Commande;

public class ActiverReservationCommande implements Commande {

    public void execute() {
        ReservationControleur.avecReservation();
    }
}
