package miage.vswk.utils.menu.commandes;

import miage.vswk.utils.ClientSingleton;
import miage.vswk.utils.Insertion;
import miage.vswk.utils.menu.Commande;

import static miage.vswk.utils.Texte.afficher;

public class ModifierMailCommande implements Commande {
    @Override
    public void execute() {
        afficher("Veuillez insérer votre adresse mail :");
        ClientSingleton.getInstance().getClient().setMail(Insertion.getMail());
    }
}
