package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.ClientControleur;
import miage.vswk.controleur.PrincipalControleur;
import miage.vswk.metier.Client;
import miage.vswk.utils.ClientSingleton;
import miage.vswk.utils.menu.Commande;

public class InscriptionCommande implements Commande {
    @Override
    public void execute() {
        ClientControleur.inscription();
        Client client = ClientSingleton.getInstance().getClient();
        if (client == null) {
            PrincipalControleur.lancement();
        }
    }
}
