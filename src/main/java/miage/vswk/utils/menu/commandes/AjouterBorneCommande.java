package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.BorneControleur;
import miage.vswk.utils.menu.Commande;

public class AjouterBorneCommande implements Commande {
    @Override
    public void execute() {
        BorneControleur.ajouterBorne();
    }
}
