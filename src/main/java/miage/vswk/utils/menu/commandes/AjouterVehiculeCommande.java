package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.VoitureControleur;
import miage.vswk.utils.menu.Commande;

public class AjouterVehiculeCommande implements Commande {
    @Override
    public void execute() {
        VoitureControleur.insertion(false);
    }
}
