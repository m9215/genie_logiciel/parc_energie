package miage.vswk.utils.menu.commandes;

import miage.vswk.metier.Parametre;
import miage.vswk.utils.Insertion;
import miage.vswk.utils.menu.Commande;

import static miage.vswk.utils.Texte.afficher;

public class ModifierPrixDepassementCommande implements Commande {
    private final Parametre parametres;

    public ModifierPrixDepassementCommande(Parametre parametres) {
        this.parametres = parametres;
    }

    @Override
    public void execute() {
        afficher("Veuillez saisir le nouveau prix de dépassement :");
        parametres.setPrixDepassement(Insertion.getDouble());
    }
}
