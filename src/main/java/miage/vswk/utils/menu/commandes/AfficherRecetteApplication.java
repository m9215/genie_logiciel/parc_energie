package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.AdministrateurControleur;
import miage.vswk.utils.menu.Commande;

public class AfficherRecetteApplication implements Commande {
    @Override
    public void execute() {
        AdministrateurControleur.afficherRecetteApplication();
    }
}
