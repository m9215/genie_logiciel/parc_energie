package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.ReservationControleur;
import miage.vswk.utils.menu.Commande;

public class UtiliserBorneSansReservationCommande implements Commande {
    @Override
    public void execute() {
        ReservationControleur.sansReservation();
    }
}
