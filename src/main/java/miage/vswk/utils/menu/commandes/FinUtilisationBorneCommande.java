package miage.vswk.utils.menu.commandes;

import miage.vswk.controleur.BorneControleur;
import miage.vswk.utils.menu.Commande;

public class FinUtilisationBorneCommande implements Commande {
    @Override
    public void execute() {
        BorneControleur.arretUtilisationBorne();
    }
}
