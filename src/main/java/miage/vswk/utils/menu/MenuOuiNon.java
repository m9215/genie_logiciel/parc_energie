package miage.vswk.utils.menu;

import miage.vswk.utils.Insertion;

import static miage.vswk.utils.Texte.afficher;

public class MenuOuiNon {

    private MenuOuiNon() {
    }

    public static boolean launch() {
        afficher("""
                            \t[1] Oui
                            \t[2] Non
                            """);
        int choix = Insertion.getInt();
        if (choix != 1 && choix != 2) {
            afficher("Veuillez entrer un choix valide.\n");
            return launch();
        }
        return choix == 1;
    }
}
