package miage.vswk.utils.menu;

import miage.vswk.controleur.PrincipalControleur;
import miage.vswk.utils.Insertion;

import java.util.List;

import static miage.vswk.utils.Texte.afficher;
import static miage.vswk.utils.Texte.afficherSansRetourLigne;

public record MenuListe<T>(String nom, List<T> liste) {

    public Object launch() {
        afficher(this.nom + " :");
        for (int i = 0; i < liste.size(); i++) {
            afficher("\t[" + (i + 1) + "] : " + liste.get(i));
        }
        afficher("\t[" + (liste.size()+1) + "] : Retour");

        afficherSansRetourLigne("Votre choix : ");
        int choix = Insertion.getInt();

        if (choix == liste().size()+1) {
            PrincipalControleur.afficherMenuPrincipal();
        }

        try {
            return liste.get(choix - 1);
        } catch (IndexOutOfBoundsException e) {
            afficher("Merci de choisir une option valide.\n");
            this.launch();
        }
        return null;
    }
}
