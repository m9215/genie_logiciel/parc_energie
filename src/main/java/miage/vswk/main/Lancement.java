package miage.vswk.main;

import miage.vswk.controleur.ClientControleur;
import miage.vswk.controleur.LocationController;
import miage.vswk.controleur.PrincipalControleur;
import miage.vswk.controleur.ReservationControleur;

import java.time.LocalDateTime;
import java.util.Timer;
import java.util.TimerTask;

public class Lancement {

    private static final Timer timerCheckReservation = new Timer();

    private static final Timer timerSendFacturation = new Timer();
    private static final Timer timerSendNotification = new Timer();
    private static final Timer timerCheckPeriodeAttente = new Timer();
    private static final Timer timerCheckLocation = new Timer();

    public static void main(String[] args) {

        timerSendFacturation.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Commenter la condition pour envoyer les factures à tout moment
                if (LocalDateTime.now().getDayOfMonth() == 1)
                    ClientControleur.envoyerFactureToAllClients();
            }
        }, 0, (long) 24 * 3600 * 1000);

        timerCheckReservation.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Passe les réservations "En attente" à "Terminée" et les factures
                ReservationControleur.checkReservation();
            }
        }, 0, (long) 60 * 1000);

        timerSendNotification.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Envoie une notification au client si sa réservation est arrivée à échéance
                ReservationControleur.notificationEcheance();
            }
        }, 0, (long) 60 * 1000);

        timerCheckPeriodeAttente.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Mets en état "Manquée" si le client ne s'est pas présenté en borne avant la fin de sa période d'attente
                ReservationControleur.finPeriodeAttente();
            }
        }, 0, (long) 60 * 1000);

        timerCheckLocation.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                LocationController.checkLocation();

            }
        }, 0, (long) 2 * 3600 * 1000);


        PrincipalControleur.lancement();


    }
}
