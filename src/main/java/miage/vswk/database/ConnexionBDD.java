package miage.vswk.database;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.logging.Logger;

public class ConnexionBDD {

    private static final Logger LOG = Logger.getLogger(ConnexionBDD.class.getName());

    private static ConnexionBDD instance;
    private java.sql.Connection connexion;

    private ConnexionBDD() {
        HashMap<String, String> config = new HashMap<>();
        try (FileReader fr = new FileReader("src/main/sql/bdd.proprietes");
             BufferedReader br = new BufferedReader(fr)) {
            String line;
            while ((line = br.readLine()) != null) {
                config.put(line.split("=")[0], line.split("=")[1]);
            }
        } catch (Exception e) {
            LOG.severe("Erreur de lecture du fichier de configuration");
        }
        try {
           this.connexion = DriverManager.getConnection(
                   config.get("URL"),
                   config.get("UTILISATEUR"),
                   config.get("MDP")
           );
        } catch (Exception e) {
            LOG.severe("Erreur de connexion à la base de données");
        }
    }

    /**
     * Singleton pattern
     *
     * @return instance of DatabaseConnexion
     */
    public static ConnexionBDD getInstance() {
        if (instance == null) {
            instance = new ConnexionBDD();
        }
        try {
            if (instance.connexion != null) {
                instance = new ConnexionBDD();
            }
        } catch (Exception e) {
            LOG.severe("Erreur de recuperation de la base de données");
        }
        return instance;
    }


    public Connection getConnexion() {
        return this.connexion;
    }
}
