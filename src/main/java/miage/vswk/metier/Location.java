package miage.vswk.metier;

import java.sql.Timestamp;

public class Location {

    private final int id;
    private final int clientId;
    private final int voitureId;
    private final Timestamp dateDebut;

    public Location(int id, int clientId, int voitureId, Timestamp dateDebut) {
        this.id = id;
        this.clientId = clientId;
        this.voitureId = voitureId;
        this.dateDebut = dateDebut;
    }

    public Location(int clientId, int voitureId, Timestamp dateDebut) {
        this(-1, clientId, voitureId, dateDebut);
    }

    public int getId() {
        return id;
    }


    public int getClientId() {
        return clientId;
    }

    public int getVoitureId() {
        return voitureId;
    }

    public Timestamp getDateDebut() {
        return dateDebut;
    }

}
