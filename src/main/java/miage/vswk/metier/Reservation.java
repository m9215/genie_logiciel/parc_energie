package miage.vswk.metier;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Reservation {

    private final int id;
    private final int clientId;
    private int borneId;
    private final int voitureId;
    private final Timestamp dateDebut;
    private Timestamp dateFin;
    private String statut;
    private Double prixFinal;

    private int nombreProlongations;
    private int nombreProlongationsAttente;

    public Reservation(int id, int clientId, int borneId, int voitureId, Timestamp dateDebut, Timestamp dateFin, String statut, Double prixFinal) {
        this.id = id;
        this.clientId = clientId;
        this.borneId = borneId;
        this.voitureId = voitureId;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.statut = statut;
        this.prixFinal = prixFinal;
        this.nombreProlongations = 0;
        this.nombreProlongationsAttente = 1;
    }

    public Reservation (int clientId, int borneId, int voitureId, Timestamp dateDebut, Timestamp dateFin) {
        this.id = -1;
        this.clientId = clientId;
        this.borneId = borneId;
        this.voitureId = voitureId;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;

        if (dateDebut.before(Timestamp.valueOf(LocalDateTime.now()))) {
            this.statut = "En cours";
        } else {
            this.statut = "En attente";
        }
    }

    public Reservation(int id, int clientId, int borneId, int voitureId, Timestamp dateDebut, Timestamp dateFin, String statut, double prixFinal, int nombreProlongation, int nombreProlongationsAttente) {
        this.id = id;
        this.clientId = clientId;
        this.borneId = borneId;
        this.voitureId = voitureId;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.statut = statut;
        this.prixFinal = prixFinal;
        this.nombreProlongations = nombreProlongation;
        this.nombreProlongationsAttente = nombreProlongationsAttente;
	}
    public boolean estValide() {
        return dateFin.after(dateDebut);
    }

    public int getId() {
        return id;
    }


    public int getClientId() {
        return clientId;
    }

    public int getBorneId() {
        return borneId;
    }

    public void setBorneId(int borneId) {
        this.borneId = borneId;
    }

    public int getVoitureId() {
        return voitureId;
    }

    public Timestamp getDateDebut() {
        return dateDebut;
    }

    public Timestamp getDateFin() {
        return dateFin;
    }

    public void setDateFin(Timestamp dateFin) {
        this.dateFin = dateFin;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public Double getPrixFinal() {
        return prixFinal;
    }

    public void setPrixFinal(Double prixFinal) {
        this.prixFinal = prixFinal;
    }

    @Override
    public String toString() {
        return "Reservation n°" + this.getId() + " (" + this.statut + ")";
    }

    public int getNombreProlongations() {
        return nombreProlongations;
    }

    public void setNombreProlongations(int nombreProlongations) {
        this.nombreProlongations = nombreProlongations;
    }

    public boolean incrementerNombreProlongations() {
        if (this.nombreProlongations < 3) {
            this.nombreProlongations++;
            return true;
        }
        return false;
    }

    public int getNombreProlongationsAttente() {
        return nombreProlongationsAttente;
    }

    public void setNombreProlongationsAttente(int nombreProlongationsAttente) {
        this.nombreProlongationsAttente = nombreProlongationsAttente;
    }
}
