package miage.vswk.metier;

public class Parametre {

    private Double prix;
    private Double prixDepassement;
    private Double prixDedommagement;
    private int tempsAttente;
    private int tempsLocation;
    private Double prixElectricite;
    private Double prixSupplement;

    public Parametre(Double prix, Double prixDepassement, Double prixDedommagement, int tempsAttente, int tempsLocation, Double prixElectricite, Double prixSupplement) {
        this.prix = prix;
        this.prixDepassement = prixDepassement;
        this.prixDedommagement = prixDedommagement;
        this.tempsAttente = tempsAttente;
        this.tempsLocation = tempsLocation;
        this.prixElectricite = prixElectricite;
        this.prixSupplement = prixSupplement;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public Double getPrixDepassement() {
        return prixDepassement;
    }

    public void setPrixDepassement(Double prixDepassement) {
        this.prixDepassement = prixDepassement;
    }

    public Double getPrixDedommagement() {
        return prixDedommagement;
    }

    public void setPrixDedommagement(Double prixDedommagement) {
        this.prixDedommagement = prixDedommagement;
    }

    public int getTempsAttente() {
        return tempsAttente;
    }

    public void setTempsAttente(int tempsAttente) {
        this.tempsAttente = tempsAttente;
    }

    public int getTempsLocation() {
        return tempsLocation;
    }

    public void setTempsLocation(int tempsLocation) {
        this.tempsLocation = tempsLocation;
    }

    public Double getPrixElectricite() { return prixElectricite; }

    public void setPrixElectricite(Double prixElectricite) { this.prixElectricite = prixElectricite; }

    public Double getPrixSupplement() {
        return prixSupplement;
    }

    public void setPrixSupplement(Double prixSupplement) {
        this.prixSupplement = prixSupplement;
    }
}
