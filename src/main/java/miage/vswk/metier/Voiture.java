package miage.vswk.metier;

public class Voiture {

    private int id;
    private final int clientId;
    private final String plaqueImmatriculation;

    public Voiture(int id, int clientId, String plaqueImmatriculation) {
        this.id = id;
        this.clientId = clientId;
        this.plaqueImmatriculation = plaqueImmatriculation;
    }

    public Voiture(int clientId, String plaqueImmatriculation) {
        this.clientId = clientId;
        this.plaqueImmatriculation = plaqueImmatriculation;
    }

    public int getId() {
        return id;
    }


    public int getClientId() {
        return clientId;
    }

    public String getPlaqueImmatriculation() {
        return plaqueImmatriculation;
    }

    @Override
    public String toString() {
        return "Voiture : " + this.getPlaqueImmatriculation();
    }

    public void setId(int anInt) {
        this.id = anInt;
    }
}
