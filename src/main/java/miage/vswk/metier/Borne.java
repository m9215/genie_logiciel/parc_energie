package miage.vswk.metier;

import miage.vswk.utils.Texte;

public class Borne {

    private final int id;
    private String statutBorne;

    public Borne(int id, String statutBorne) {
        this.id = id;
        this.statutBorne = statutBorne;
    }

    public Borne(String statut) {
        this.id = -1;
        this.statutBorne = statut;
    }

    public int getId() {
        return id;
    }


    public String getStatutBorne() {
        return statutBorne;
    }

    public void setStatutBorne(String statutBorne) {
        this.statutBorne = statutBorne;
    }

    @Override
    public String toString() {
        return Texte.BORNE_NUMERO + id + " (" + statutBorne + ")";
    }
}