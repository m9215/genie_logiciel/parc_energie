package miage.vswk.metier;

public class Client {

    private int id;
    private String nom;
    private String prenom;
    private String mobile;
    private String mail;
    private String cb;
    private final boolean estAdmin;

    public Client(int id, String nom, String prenom, String mail, String mobile, String cb, boolean estAdmin) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.mobile = mobile;
        this.mail = mail;
        this.cb = cb;
        this.estAdmin = estAdmin;
    }

    public Client(String nom, String prenom, String mail, String mobile, String cb, boolean estAdmin) {
        this.nom = nom;
        this.prenom = prenom;
        this.mobile = mobile;
        this.mail = mail;
        this.cb = cb;
        this.estAdmin = estAdmin;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMobile() {
        return mobile;
    }
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getCb() {
        return cb;
    }

    public void setCb(String cb) {
        this.cb = cb;
    }

    public boolean isEstAdmin() {
        return estAdmin;
    }

    public boolean estValide() {
        return nom != null && !nom.isEmpty()
                && prenom != null && !prenom.isEmpty()
                && mail != null && !mail.isEmpty()
                && mobile != null && !mobile.isEmpty()
                && cb != null && !cb.isEmpty();
    }

    @Override
    public String toString() {
        return "Client n°" + this.getId() + " : " + this.getNom() + " " + this.getPrenom() + " (" + this.getMail() + ")";
    }
}