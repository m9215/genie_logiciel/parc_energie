create table if not exists borne
(
    id           int auto_increment
        primary key,
    statut_borne enum ('Occupée', 'Disponible', 'Indisponible') null
)
    engine = InnoDB;

create table if not exists client
(
    id           int auto_increment
        primary key,
    nom          varchar(255) null,
    prenom       varchar(255) null,
    mail         varchar(255) null,
    mobile       varchar(255) null,
    carte_credit varchar(255) null
)
    engine = InnoDB;

create table if not exists parametres
(
    prix             double null,
    prix_depassement double null,
    temps_attente    int    null,
    temps_location   int    null
)
    engine = InnoDB;

create table if not exists voiture
(
    id                     int auto_increment
        primary key,
    client_id              int          null,
    plaque_immatriculation varchar(255) null,
    constraint voiture_ibfk_1
        foreign key (client_id) references client (id)
)
    engine = InnoDB;

create table if not exists location
(
    id         int auto_increment
        primary key,
    client_id  int       null,
    voiture_id int       null,
    date_debut timestamp null,
    constraint location_ibfk_1
        foreign key (client_id) references client (id),
    constraint location_ibfk_2
        foreign key (voiture_id) references voiture (id)
)
    engine = InnoDB;

create index client_id
    on location (client_id);

create index voiture_id
    on location (voiture_id);

create table if not exists reservation
(
    id         int auto_increment
        primary key,
    client_id  int                                                    null,
    borne_id   int                                                    null,
    voiture_id int                                                    null,
    date_debut timestamp                                              null,
    date_fin   timestamp                                              null,
    statut     enum ('En attente', 'Terminée', 'Annulée', 'En cours') null,
    prix_final double                                                 null,
    constraint reservation_ibfk_1
        foreign key (client_id) references client (id),
    constraint reservation_ibfk_2
        foreign key (borne_id) references borne (id),
    constraint reservation_ibfk_3
        foreign key (voiture_id) references voiture (id)
)
    engine = InnoDB;

create index borne_id
    on reservation (borne_id);

create index client_id
    on reservation (client_id);

create index voiture_id
    on reservation (voiture_id);

create index client_id
    on voiture (client_id);

INSERT INTO client (nom, prenom, mail, mobile, carte_credit, est_admin) VALUES ('Jean', 'Dupont', 'admin@parc.fr', '0602134354', '1234 5667 3324 3255 2345', 1);

INSERT INTO parametres (prix, prix_depassement, temps_attente, temps_location, prix_dedommagement, prix_electricite, prix_supplement) VALUES (10, 10, 10, 10, 10, 10, 10);