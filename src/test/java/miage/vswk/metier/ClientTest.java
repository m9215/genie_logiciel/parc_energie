package miage.vswk.metier;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ClientTest {

    @Test
    void estValideFalse() {
        assertFalse(new Client(0, "", "", "", "", "", false).estValide());
        assertFalse(new Client(0, "", null, "", "", "", true).estValide());
    }

    @Test
    void estValideTrue() {
        assertTrue(new Client(0, "nom", "prenom", "mail", "mobile", "cb", true).estValide());
        assertTrue(new Client(0, "nom", "prenom", "mail", "mobile", "cb", false).estValide());
    }
}