package miage.vswk.metier;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.*;

class ReservationTest {

    @Test
    @DisplayName("Fonctionnement incrementerNombreProlongations : OK")
    void incrementerNombreProlongations() {
        Reservation reservation = new Reservation(1, 1, 1, 1, Timestamp.valueOf("2022-05-23 20:22:22"), Timestamp.valueOf("2022-05-23 22:22:22"), "En cours", 1.0);
        assertTrue(reservation.incrementerNombreProlongations());
        assertEquals(1, reservation.getNombreProlongations());
    }

    @Test
    @DisplayName("Fonctionnement incrementerNombreProlongations : nombre de réservations > 3")
    void getNombreProlongationsDepasse() {
        Reservation reservation = new Reservation(1, 1, 1, 1, Timestamp.valueOf("2022-05-23 20:22:22"), Timestamp.valueOf("2022-05-23 22:22:22"), "En cours", 1.0);
        reservation.setNombreProlongations(3);
        assertFalse(reservation.incrementerNombreProlongations());
    }

    @Test
    void estValideFalse() {
        assertFalse(new Reservation(0, 0, 0, 0, Timestamp.valueOf("2022-05-13 19:00:00"), Timestamp.valueOf("2022-05-13 18:00:00"), "Disponible", 0.0).estValide());
        assertFalse(new Reservation(0, 0, 0, 0, Timestamp.valueOf("2022-05-13 20:00:00"), Timestamp.valueOf("2022-05-13 18:00:00"), "En cours", 0.0).estValide());
    }

    @Test
    void estValideTrue() {
        assertTrue(new Reservation(0, 0, 0, 0, Timestamp.valueOf("2022-05-13 18:00:00"), Timestamp.valueOf("2022-05-13 19:00:00"), "Disponible", 0.0).estValide());
        assertTrue(new Reservation(0, 0, 0, 0, Timestamp.valueOf("2022-05-13 18:00:00"), Timestamp.valueOf("2022-05-13 20:00:00"), "En cours", 0.0).estValide());
    }

}
