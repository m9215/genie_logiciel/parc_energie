package miage.vswk.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InsertionTest {

    // **********************
    // TEST IMMATRICULATION
    // ***********************


    @TestFactory
    @DisplayName("Vérification plaque immatriculation fausse")
    Stream<DynamicTest> plaqueImmatriculationFalse() {
        List<String> immatriculations = List.of(
                "AA-848-Z4",
                "AA-848-dz",
                "AA-84d-DZ",
                "A4-848-DZ",
                "AA-8458-DZ",
                "AA-848-DZZ",
                "Azz-848-DZ",
                "AA-88-DZ",
                "AAd-8z48-DaZ",
                "AAA-848-DZa",
                ""
        );
        return immatriculations.stream().map(immatriculation -> DynamicTest.dynamicTest(
                "Test de la plaque d'immatriculation " + immatriculation,
                () -> assertFalse(Insertion.verifPlaqueImmat(immatriculation))
        ));
    }

    @Test
    @DisplayName("Vérification plaque immatriculation nulle")
    void plaqueImmatriculationNull() {
        assertFalse(Insertion.verifPlaqueImmat(null));
    }

    @Test
    @DisplayName("Vérification plaque immatriculation vraie")
    void plaqueImmatriculationTrue() {
        assertTrue(Insertion.verifPlaqueImmat("AA-848-DZ"));
    }


    // **********************
    // TEST EMAIL
    // ***********************

    @TestFactory
    @DisplayName("Vérification email fausse")
    Stream<DynamicTest> emailFalse() {
        List<String> emails = List.of(
                "",
                "a",
                "a@",
                "a@a",
                "a@a."
        );
        return emails.stream().map(email -> DynamicTest.dynamicTest(
                "Test de l'email " + email,
                () -> assertFalse(Insertion.verifMail(email))
        ));
    }

    @Test
    @DisplayName("Vérification email nulle")
    void emailNull() {
        assertFalse(Insertion.verifMail(null));
    }

    @Test
    @DisplayName("Vérification email vraie")
    void emailTrue() {
        assertTrue(Insertion.verifMail("test@test.fr"));
    }


    // **********************
    // TEST NUMERO TEL
    // ************************

    @TestFactory
    @DisplayName("Vérification numéro de téléphone fausse")
    Stream<DynamicTest> numeroTelFalse() {
        List<String> numeroTels = List.of(
                "0FEZFEZF",
                "0 03 32 43 45",
                "+ 21 43 12 54 23"
        );
        return numeroTels.stream().map(numeroTel -> DynamicTest.dynamicTest(
                "Test du numéro de téléphone " + numeroTel,
                () -> assertFalse(Insertion.verifNumeroTel(numeroTel))
        ));
    }

    @Test
    @DisplayName("Vérification numéro de téléphone nulle")
    void numeroTelNull() {
        assertFalse(Insertion.verifNumeroTel(null));
    }

    @Test
    @DisplayName("Vérification numéro de téléphone vraie")
    void numeroTelTrue() {
        assertTrue(Insertion.verifNumeroTel("09 03 32 43 45"));
    }


    // **********************
    // TEST CARDE DE CREDIT NUMERO
    // ***********************

    @TestFactory
    @DisplayName("Vérification numéro de carte de crédit fausse")
    Stream<DynamicTest> numeroCarteCreditFalse() {
        List<String> numeroCarteCredits = List.of(
                "0FEZFEZF",
                "0 03 32 43 45",
                "+ 21 43 12 54 23",
                "1234-5678-9012-3456"
        );
        return numeroCarteCredits.stream().map(numeroCarteCredit -> DynamicTest.dynamicTest(
                "Test du numéro de carte de crédit " + numeroCarteCredit,
                () -> assertFalse(Insertion.verifNumeroCarte(numeroCarteCredit))
        ));
    }

    @Test
    @DisplayName("Vérification numéro de carte de crédit nulle")
    void numeroCarteCreditNull() {
        assertFalse(Insertion.verifNumeroCarte(null));
    }

    @Test
    @DisplayName("Vérification numéro de carte de crédit vraie")
    void numeroCarteCreditTrue() {
        assertTrue(Insertion.verifNumeroCarte("1234 5678 9012 3456 7890"));
    }

}
