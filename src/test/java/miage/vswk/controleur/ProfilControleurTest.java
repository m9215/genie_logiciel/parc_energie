package miage.vswk.controleur;

import miage.vswk.metier.Client;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ProfilControleurTest {

    @Test
    @DisplayName("Test avec une liste vide")
    void verifierListeClient() {
        assertFalse(ProfilControleur.verifierListeClient(new ArrayList<>()));
    }

    @Test
    @DisplayName("Test avec une liste null")
    void verifierListeClientNull() {
        assertFalse(ProfilControleur.verifierListeClient(null));
    }

    @Test
    @DisplayName("Test avec une liste non vide")
    void verifierListeClientNonVide() {
        List<Client> clients = new ArrayList<>();
        clients.add(new Client("nom", "prenom", "mail", "mobile", "cb", false));
        clients.add(new Client("nom2", "prenom2", "mail2", "mobile2", "cb2", false));
        assertTrue(ProfilControleur.verifierListeClient(clients));
    }
}