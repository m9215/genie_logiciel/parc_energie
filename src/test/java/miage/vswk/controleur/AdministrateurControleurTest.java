package miage.vswk.controleur;

import miage.vswk.dao.ReservationDAO;
import miage.vswk.metier.Reservation;
import miage.vswk.utils.Texte;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mockStatic;

class AdministrateurControleurTest {

    // Verification de la valeur retourn
    @Test
    void calculerRecetteApplication() {
        try (MockedStatic<ReservationDAO> mocked = mockStatic(ReservationDAO.class)) {
            Timestamp debut = new Timestamp(System.currentTimeMillis() + 3600000);
            Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);

            List<Reservation> listeTerminee = new ArrayList<>();
            List<Reservation> listeManquee = new ArrayList<>();
            List<Reservation> listeAnnulee = new ArrayList<>();
            // Terminées
            listeTerminee.add(new Reservation(1, 1, 1, 1, debut, fin, "Terminée", 20, 0,1));
            listeTerminee.add(new Reservation(2, 1, 1, 1, debut, fin, "Terminée", 10, 0,1));
            // Annulées
            // Les annulées peuvent être négatives, dans le cas d'un dédommagement par exemple
            listeAnnulee.add(new Reservation(1, 1, 1, 1, debut, fin, "Annulée", -20, 0,1));
            listeAnnulee.add(new Reservation(2, 1, 1, 1, debut, fin, "Annulée", 10, 0,1));
            // Manquées
            listeManquee.add(new Reservation(1, 1, 1, 1, debut, fin, "Manquée", 20, 0,1));
            listeManquee.add(new Reservation(2, 1, 1, 1, debut, fin, "Manquée", 10, 0,1));

            mocked.when(() -> ReservationDAO.getReservationByStatut(Texte.TERMINEE)).thenReturn(listeTerminee);
            mocked.when(() -> ReservationDAO.getReservationByStatut(Texte.MANQUEE)).thenReturn(listeManquee);
            mocked.when(() -> ReservationDAO.getReservationByStatut(Texte.ANNULEE)).thenReturn(listeAnnulee);

            Double recettes = AdministrateurControleur.calculerRecetteApplication();
            Double recettesExpected = 50.0;

            assertEquals(recettesExpected, recettes);

        }
    }
}