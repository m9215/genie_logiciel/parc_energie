package miage.vswk.controleur;

import miage.vswk.dao.ParametreDAO;
import miage.vswk.dao.ReservationDAO;
import miage.vswk.metier.*;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mockStatic;

class ReservationControleurTest {
    // Voiture non trouvée : null
    @Test
    void creationReservationVoitureNull() {

        Borne b = new Borne("Disponible");
        Client client = new Client("nom", "prenom", "mail", "mobile", "cb", false);
        Timestamp debut = new Timestamp(System.currentTimeMillis() + 3600000);
        Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);
        assertNull(ReservationControleur.creationReservation(null, b, client, debut, fin));
    }


    // Borne non trouvée : null
    @Test
    void creationReservationBorneNull() {
        Voiture v = new Voiture(1, "AA-111-AA");
        Client client = new Client("nom", "prenom", "mail", "mobile", "cb", false);
        Timestamp debut = new Timestamp(System.currentTimeMillis() + 3600000);
        Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);
        assertNull(ReservationControleur.creationReservation(v, null, client, debut, fin));
    }

    // Reservation correcte
    @Test
    void creationReservationCorrect() {
        Borne b = new Borne("Disponible");
        Voiture v = new Voiture(1, "AA-111-AA");
        Client client = new Client("nom", "prenom", "mail", "mobile", "cb", false);
        Timestamp debut = new Timestamp(System.currentTimeMillis() + 3600000);
        Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);
        assertInstanceOf(Reservation.class,ReservationControleur.creationReservation(v, b, client, debut, fin));
    }


    // debut avant aujourd'hui : null
    @Test
    void creationReservationDatePassee() {
        Borne b = new Borne("Disponible");
        Voiture v = new Voiture(1, "AA-111-AA");
        Client client = new Client("nom", "prenom", "mail", "mobile", "cb", false);
        Timestamp debut = new Timestamp(System.currentTimeMillis() - 3600000);
        Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);
        assertNull(ReservationControleur.creationReservation(v, b, client, debut, fin));
    }

    // fin avant debut : null
    @Test
    void creationReservationDatesEchangees() {
        Borne b = new Borne("Disponible");
        Voiture v = new Voiture(1, "AA-111-AA");
        Client client = new Client("nom", "prenom", "mail", "mobile", "cb", false);
        Timestamp debut = new Timestamp(System.currentTimeMillis() - 3600000);
        Timestamp fin = new Timestamp(System.currentTimeMillis() - 7200000);
        assertNull(ReservationControleur.creationReservation(v, b, client, debut, fin));
    }

    // Il n'y a aucune réservation annulée : 0.0
    @Test
    void checkDedommagementClientEmpty() {
        try (MockedStatic<ReservationDAO> mocked = mockStatic(ReservationDAO.class)) {
            List<Reservation> liste = new ArrayList<>();
            mocked.when(() -> ReservationDAO.getReservationByStatut("Annulée")).thenReturn(liste);
            Client client = new Client("nom", "prenom", "mail", "mobile", "cb", false);
            Double dedommagement = 0.0;
            assertThat(ReservationControleur.checkDedommagementClient(client)).isEqualTo(dedommagement);
        }
    }

    // Il y a au moins une réservation Annulée pour le client, mais pas à dédommager : 0.0
    @Test
    void checkDedommagementClientNotEmpty_AucunDedommagement() {
        try (MockedStatic<ReservationDAO> mocked = mockStatic(ReservationDAO.class)) {
            Client client = new Client("nom", "prenom", "mail", "mobile", "cb", false);
            Timestamp debut = new Timestamp(System.currentTimeMillis() + 3600000);
            Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);
            List<Reservation> liste = new ArrayList<>();
            liste.add(new Reservation(1, client.getId(), 1, 1, debut, fin, "Annulée", 0, 0,1));
            mocked.when(() -> ReservationDAO.getReservationByStatut("Annulée")).thenReturn(liste);
            Double dedommagement = 0.0;
            assertThat(ReservationControleur.checkDedommagementClient(client)).isEqualTo(dedommagement);
        }
    }

    // Il y a une réservation à dédommager, mais pas pour ce client : 0.0
    @Test
    void checkDedommagementClientNotEmpty_AucunDedommagementPourClient() {
        try (MockedStatic<ReservationDAO> mocked = mockStatic(ReservationDAO.class)) {
            Client client = new Client("nom", "prenom", "mail", "mobile", "cb", false);
            Timestamp debut = new Timestamp(System.currentTimeMillis() + 3600000);
            Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);

            int id = 1;
            if (client.getId() != 2)
                id = 2;
            List<Reservation> liste = new ArrayList<>();
            liste.add(new Reservation(1, id, 1, 1, debut, fin, "Annulée", -6, 0,1));

            mocked.when(() -> ReservationDAO.getReservationByStatut("Annulée")).thenReturn(liste);
            Double dedommagement = 0.0;
            assertThat(ReservationControleur.checkDedommagementClient(client)).isEqualTo(dedommagement);
        }
    }


    // Il y a une réservation à dédommager pour ce client de 6euros : -6.0
    @Test
    void checkDedommagementClientNotEmpty_DedommagementPourClient() {
        try (MockedStatic<ReservationDAO> mocked = mockStatic(ReservationDAO.class)) {
            Client client = new Client("nom", "prenom", "mail", "mobile", "cb", false);
            Timestamp debut = new Timestamp(System.currentTimeMillis() + 3600000);
            Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);

            List<Reservation> liste = new ArrayList<>();
            liste.add(new Reservation(1, client.getId(), 1, 1, debut, fin, "Annulée", -6, 0,1));

            mocked.when(() -> ReservationDAO.getReservationByStatut("Annulée")).thenReturn(liste);
            Double dedommagement = -6.0;
            assertThat(ReservationControleur.checkDedommagementClient(client)).isEqualTo(dedommagement);
        }
    }

    // Il y a des réservations à dédommager pour ce client de 6euros et 5euros : -11.0
    @Test
    void checkDedommagementClientNotEmpty_DedommagementMultiplePourClient() {
        try (MockedStatic<ReservationDAO> mocked = mockStatic(ReservationDAO.class)) {
            Client client = new Client("nom", "prenom", "mail", "mobile", "cb", false);
            Timestamp debut = new Timestamp(System.currentTimeMillis() + 3600000);
            Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);

            List<Reservation> liste = new ArrayList<>();
            liste.add(new Reservation(1, client.getId(), 1, 1, debut, fin, "Annulée", -6, 0,1));
            liste.add(new Reservation(2, client.getId(), 1, 1, debut, fin, "Annulée", -5, 0,1));

            mocked.when(() -> ReservationDAO.getReservationByStatut("Annulée")).thenReturn(liste);
            Double dedommagement = -11.0;
            assertThat(ReservationControleur.checkDedommagementClient(client)).isEqualTo(dedommagement);
        }
    }

    // Il y a des réservations à dédommager pour ce client de 6euros et 5euros mais aussi pour un atre client : -11.0
    @Test
    void checkDedommagementClientNotEmpty_DedommagementMultiplePourClientEtAutre() {
        try (MockedStatic<ReservationDAO> mocked = mockStatic(ReservationDAO.class)) {
            Client client = new Client("nom", "prenom", "mail", "mobile", "cb", false);
            Timestamp debut = new Timestamp(System.currentTimeMillis() + 3600000);
            Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);

            int id = 1;
            if (client.getId() != 2)
                id = 2;

            List<Reservation> liste = new ArrayList<>();
            liste.add(new Reservation(1, client.getId(), 1, 1, debut, fin, "Annulée", -6, 0,1));
            liste.add(new Reservation(2, client.getId(), 1, 1, debut, fin, "Annulée", -5, 0,1));
            liste.add(new Reservation(3, id, 1, 1, debut, fin, "Annulée", -5, 0,1));

            mocked.when(() -> ReservationDAO.getReservationByStatut("Annulée")).thenReturn(liste);
            Double dedommagement = -11.0;
            assertThat(ReservationControleur.checkDedommagementClient(client)).isEqualTo(dedommagement);
        }
    }
    

    // Temps d'attente dépassé : false
    @Test
    void verifierTempsAttente_TimeOut() {
        try (MockedStatic<ParametreDAO> mocked = mockStatic(ParametreDAO.class)) {
            //int tempsAttente = 15;
            mocked.when(ParametreDAO::getParametres).thenReturn(new Parametre(1.0,1.0,1.0,15,1,1.0,1.0));

            // debut y'a 20 minutes
            Timestamp debut = new Timestamp(System.currentTimeMillis() - 1200000);
            Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);

            Reservation reservation = new Reservation(1, 1, 1, 1, debut, fin, "En cours", -5, 0, 1);

            assertFalse(ReservationControleur.verifierTempsAttente(reservation));
        }
    }

    // Temps d'attente non dépassé : true
    @Test
    void verifierTempsAttente_Time(){
        try (MockedStatic<ParametreDAO> mocked = mockStatic(ParametreDAO.class)) {
            //int tempsAttente = 15;
            mocked.when(ParametreDAO::getParametres).thenReturn(new Parametre(1.0,1.0,1.0,15,1,1.0,1.0));

            // debut y'a 10 minutes
            Timestamp debut = new Timestamp(System.currentTimeMillis() - 600000);
            Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);

            Reservation reservation = new Reservation(1, 1, 1, 1, debut, fin, "En cours", -5, 0, 1);

            assertTrue(ReservationControleur.verifierTempsAttente(reservation));
        }
    }

    // Temps d'attente fini après dateFin réservation : false
    @Test
    void verifierTempsAttente_TimeAfterEnd() {
        try (MockedStatic<ParametreDAO> mocked = mockStatic(ParametreDAO.class)) {
            //int tempsAttente = 15;
            mocked.when(ParametreDAO::getParametres).thenReturn(new Parametre(1.0,1.0,1.0,15,1,1.0,1.0));

            // debut y'a 10 minutes
            Timestamp debut = new Timestamp(System.currentTimeMillis() - 600000);
            Timestamp fin = new Timestamp(System.currentTimeMillis() + 600000);

            Reservation reservation = new Reservation(1, 1, 1, 1, debut, fin, "En cours", -5, 0, 2);

            assertFalse(ReservationControleur.verifierTempsAttente(reservation));
        }
    }

    // Ajouter du temps d'attente
    @Test
    void ajouterTempsAttente() {
        try (MockedStatic<ParametreDAO> mocked = mockStatic(ParametreDAO.class)) {
            mocked.when(ParametreDAO::getParametres).thenReturn(new Parametre(1.0, 1.0, 1.0, 1, 1, 1.0, 10.0));

            Timestamp debut = new Timestamp(System.currentTimeMillis() - 600000);
            Timestamp fin = new Timestamp(System.currentTimeMillis() + 600000);

            Reservation reservation = new Reservation(1, 1, 1, 1, debut, fin, "En cours", 0, 0, 1);
            Reservation reservationExpected = new Reservation(1, 1, 1, 1, debut, fin, "En cours", 10, 0, 2);

            ReservationControleur.ajouterTempsAttente(reservation);

            assertThat(reservation.getNombreProlongationsAttente()).isEqualTo(reservationExpected.getNombreProlongationsAttente());
            assertThat(reservation.getPrixFinal()).isEqualTo(reservationExpected.getPrixFinal());
        }
    }

    // Incrémentation d'un horaire avec i minutes
    @Test
    void incrementerHoraire() {
        // Dans 1h
        Timestamp init = new Timestamp(System.currentTimeMillis() + 3600000);
        int minutes = 30;

        // Dans 1h30
        Timestamp resultExpected = new Timestamp(System.currentTimeMillis() + 3600000 + 1800000);

        assertThat(ReservationControleur.incrementerHoraire(init, minutes)).isEqualTo(resultExpected);
    }
    
/*

    @Test
    void fusionnerReservations_BorneIndispo_ReservationNull() {
        try (MockedStatic<BorneDAO> mocked = mockStatic(BorneDAO.class)) {
            // ARRANGE
            // -- mocking

            //Borne borne = new Borne("Disponible");
            mocked.when(() -> BorneDAO.existeBorneDisponible(any(), any(), false)).thenReturn(null);

            Timestamp debut = new Timestamp(System.currentTimeMillis() + 3600000);
            Timestamp fin = new Timestamp(System.currentTimeMillis() + 7200000);
            Reservation r = new Reservation(0, 0, 0, debut, fin);

            debut = new Timestamp(System.currentTimeMillis() + 9500000);
            fin = new Timestamp(System.currentTimeMillis() + 10800000);
            Reservation r2 = new Reservation(0, 0, 0, debut, fin);

            assertNull(ReservationControleur.fusionnerReservations(r, r2));
        }
    }
*/
//    @Test
//    public void fusionnerReservations_BorneDispo_ReservationOK() {
//        try (MockedStatic<BorneDAO> mocked = mockStatic(BorneDAO.class)) {
//            // ARRANGE
//            // -- mocking
//            Borne borne = new Borne("Disponible");
//            mocked.when(() -> BorneDAO.existeBorneDisponible(any(), any())).thenReturn(borne);
//
//            Timestamp debut1 = Timestamp.valueOf("2022-05-13 18:00:00");
//            Timestamp fin1 = Timestamp.valueOf("2022-05-13 19:00:00");
//            Reservation r = new Reservation(0, 0, 0, debut1, fin1);
//
//            Timestamp debut2 = Timestamp.valueOf("2022-05-13 19:30:00");
//            Timestamp fin2 = Timestamp.valueOf("2022-05-13 20:00:00");
//            Reservation r2 = new Reservation(0, 0, 0, debut2, fin2);
//
//            assertThat(ReservationControleur.fusionnerReservations(r, r2).DATE_DEBUT).isEqualTo(debut1);
//            assertThat(ReservationControleur.fusionnerReservations(r, r2).DATE_FIN).isEqualTo(fin2);
//            assertThat(ReservationControleur.fusionnerReservations(r, r2).getBorneId()).isEqualTo(borne.getId());
//        }
//    }


}