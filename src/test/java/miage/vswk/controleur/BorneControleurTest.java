package miage.vswk.controleur;

import miage.vswk.dao.BorneDAO;
import miage.vswk.metier.Borne;
import miage.vswk.metier.Reservation;
import miage.vswk.utils.Texte;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.sql.Timestamp;
import java.util.HashMap;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;


@ExtendWith(MockitoExtension.class)
class BorneControleurTest {


/*

    // afficherDispoBorne
    @Test
    public void isBorneDispo_BorneDispo_ExpectedTrue() {
        try (MockedStatic<BorneDAO> mocked = mockStatic(BorneDAO.class)) {
            // ARRANGE
                // mocking
            var borne = mock(Borne.class);
            mocked.when(() -> BorneDAO.existeBorneDisponible(any(), any(),false)).thenReturn(borne);

            // ACT
            var result = BorneControleur.isBorneDispo();

            // ASSERT
            assertThat(result).isTrue();
        }
    }

    @Test
    public void isBorneDispo_AucuneBorne_ExpectedFalse() {
        try (MockedStatic<BorneDAO> mocked = mockStatic(BorneDAO.class)) {
            // ARRANGE
                // mocking
            mocked.when(() -> BorneDAO.existeBorneDisponible(any(), any(), false)).thenReturn(null);

            // ACT
            var result = BorneControleur.isBorneDispo();

            // ASSERT
            assertThat(result).isFalse();
        }
    }
*/

    // gererPanne
    @Test
    void changerBorneDispoIndispo_BorneNull_ExpectedNull() {
        try (MockedStatic<BorneDAO> mocked = mockStatic(BorneDAO.class)) {
            // ARRANGE
            // mocking
            mocked.when(BorneDAO::getBornes).thenReturn(new HashMap<Integer, Borne>());

            // ACT
            var result = BorneControleur.changerBorneDispoIndispo(null);

            // ASSERT
            assertThat(result).isNull();
        }
    }

    @Test
    void gererPanne_BorneDispo_ExpectedBorneIndispo() {
        // ARRANGE
        // -- mocking DAO
        Borne borne = new Borne(Texte.DISPONIBLE);
        // ACT
        var result = BorneControleur.changerBorneDispoIndispo(borne);

        // ASSERT
        assertThat(result.getStatutBorne()).isEqualTo(Texte.INDISPONIBLE);
    }

    @Test
    void gererPanne_BorneIndispo_ExpectedBorneDispo() {
        // ARRANGE
        // -- mocking DAO
        Borne borne = new Borne("Indisponible");

        // ACT
        var result = BorneControleur.changerBorneDispoIndispo(borne);

        // ASSERT
        assertThat(result.getStatutBorne()).isEqualTo(Texte.DISPONIBLE);
    }


    // gererOccupation
    @Test
    void gererOccupation_BorneNull_ExpectedNull() {
        // ARRANGE

        // ACT
        var result = BorneControleur.gererOccupation(null);

        // ASSERT
        assertThat(result).isNull();
    }

    @Test
    void gererOccupation_BorneDispo_ExpectedOccupee() {
        // ARRANGE
        Borne borne = new Borne(Texte.DISPONIBLE);

        // ACT
        var result = BorneControleur.gererOccupation(borne);

        // ASSERT
        assertThat(result.getStatutBorne()).isEqualTo(Texte.OCCUPE);
    }

    @Test
    void gererOccupation_BorneOccupee_ExpectedDispo() {
        // ARRANGE
        Borne borne = new Borne(Texte.OCCUPE);

        // ACT
        var result = BorneControleur.gererOccupation(borne);

        // ASSERT
        assertThat(result.getStatutBorne()).isEqualTo("Disponible");
    }


    // ajouterBorne
    @Test
    void ajouterBorne_Null_ExpectedTrue() {
        try (MockedStatic<BorneDAO> mocked = mockStatic(BorneDAO.class)) {
            // ARRANGE
            // -- mocking DAO
            mocked.when(() -> BorneDAO.ajouterBorne(any())).thenAnswer(
                    new Answer<Boolean>() {
                        @Override
                        public Boolean answer(InvocationOnMock invocation) {
                            Borne b = invocation.getArgument(0);
                            return b != null;
                        }
                    });

            // ACT
            var result = BorneControleur.ajouterBorne();

            // ASSERT
            assertThat(result).isTrue();
        }
    }


    // facturerDepassement
    @Test
    void facturerDepassement_Depassement_ReturnMajoration() {
        // ARRANGE
        Timestamp dateDebut = new Timestamp(System.currentTimeMillis() - 3600000 * 2);
        Timestamp dateFin = new Timestamp(System.currentTimeMillis() - 3600000);
        Reservation reservation = new Reservation(0, 0, 0, dateDebut, dateFin);
        double tarifExpected = 1.0;

        // ACT
        var result = BorneControleur.facturerDepassement(reservation, new Timestamp(System.currentTimeMillis()), 1, true);

        // ASSERT
        assertThat(result).isEqualTo(tarifExpected);
    }

    @Test
    void facturerDepassement_PasDepassement_Return0() {
        // ARRANGE
        Timestamp dateDebut = new Timestamp(System.currentTimeMillis() - 3600000);
        Timestamp dateFin = new Timestamp(System.currentTimeMillis());
        Reservation reservation = new Reservation(0, 0, 0, dateDebut, dateFin);
        double tarifExpected = 0.0;

        // ACT
        var result = BorneControleur.facturerDepassement(reservation, new Timestamp(System.currentTimeMillis()), 1, true);

        // ASSERT
        assertThat(result).isEqualTo(tarifExpected);
    }

    @Test
    void facturerDepassement_DepartAvantFin_Return0() {
        // ARRANGE
        Timestamp dateDebut = new Timestamp(System.currentTimeMillis() - 3600000);
        Timestamp dateFin = new Timestamp(System.currentTimeMillis() + 3600000);
        Reservation reservation = new Reservation(0, 0, 0, dateDebut, dateFin);
        double tarifExpected = 0.0;

        // ACT
        var result = BorneControleur.facturerDepassement(reservation, new Timestamp(System.currentTimeMillis()), 1, true);

        // ASSERT
        assertThat(result).isEqualTo(tarifExpected);
    }


    // calculPrixByTemps
    @Test
    void calculPrixByTemps_heureDebutHeureFin_ReturnPrix() {
        // ARRANGE
        Timestamp dateDebut = new Timestamp(System.currentTimeMillis() - 3600000 * 2);
        Timestamp dateFin = new Timestamp(System.currentTimeMillis() - 3600000);
        double tarifExpected = 1.0;

        // ACT
        var result = BorneControleur.calculPrixByTemps(dateDebut, dateFin, 1);

        // ASSERT
        assertThat(result).isEqualTo(tarifExpected);
    }

    @Test
    void calculPrixByTemps_invertHeureDebutHeureFin_ReturnPrix() {
        // ARRANGE
        Timestamp dateDebut = new Timestamp(System.currentTimeMillis() - 3600000);
        Timestamp dateFin = new Timestamp(System.currentTimeMillis() - 3600000 * 2);
        double tarifExpected = 0.0;

        // ACT
        var result = BorneControleur.calculPrixByTemps(dateDebut, dateFin, 1);

        // ASSERT
        assertThat(result).isEqualTo(tarifExpected);
    }



}
