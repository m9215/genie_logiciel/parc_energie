# PROJET GÉNIE LOGICIEL
*29/05/2022*

### WEINGARTNER Olivia - SIGNER Tom - VIGNOT Benjamin - KELBERT Paul

Projet d’une durée de 4 semaines réalisé dans le cadre du **Master 1 MIAGE** à l’IDMC (= Institut des sciences du Digital, Management Cognition) de Nancy durant le cours de **Génie Logiciel**


## Réalisation du projet :
4 semaines → 4 sprints 

**Scrum** : backlog, sprint backlog, conception, tests, code 

**Git** : branches, partager code 

**SonarQube** : vérifier le code écrit en fonction des conventions 

**Refactoriser** : réécrire le code pour qu'il soit plus compréhensible par les autres développeurs 

**Trello** : lien de notre trello → https://trello.com/invite/b/eyUoJamM/ddc40c4a6609e75c0ccd4aa140293022/backlog-us-

**\doc** : Sur notre git, dans le dossier \doc se trouvent nos daily scrum, nos sprint review…


*→ Ce n’est pas l’objectif qui sera évalué, mais le cheminement.*

## Explication du projet

Le but de ce projet est de suivre et de gérer l'occupation d'un parc de bornes de recharge pour véhicules électriques et de permettre aux clients de trouver et de réserver les bornes disponibles. Les objectifs du projet sont de mettre en pratique ce que nous avons vu en cours : 

- travailler à plusieurs (groupe de 3) sur le développement d'une application,
- réussir à livrer un produit dans les temps,
- avoir le code le plus maintenable possible.

## Evolution du projet

![](doc/burndown-chart.png)

## Installation du projet

Il faut posséder une version Java 17 ainsi qu'une base de données MySQL

## Lancement de l’application

Les états de l’application après chaque commit sont récupérable via notre git à l’aide des **tags** *(sprint-1, sprint-2, sprint-3, sprint-4,...)*. Les dates approximatives sont trouvables dans notre trello.

Pour lancer l’application, lancer le **main** dans la classe **main/Lancement.java**


## Au sujet de la base de données

Nous avons fourni le script de la base de données.
Cette dernière est en langage MySQL.

Un administrateur a été inséré dans le jeu de donné (l’application ne permettant pas d’élever un utilisateur au rang d’administrateur).

Identifiant de l’administrateur :

	mail : admin@parc.fr




